

################################################################################

default: EXE NR ER CR

################################################################################

EXE:
	make --directory=rbf-tests EXE
	make --directory=rbf-inversions EXE

ER:
	make --directory=rbf-tests ER
	make --directory=rbf-inversions ER
	cp rbf-tests/Fig/*.pdf Fig/.
	cp rbf-inversions/Fig/*.pdf Fig/.

CR:
	make --directory=rbf-tests CR
	make --directory=rbf-inversions CR
	cp rbf-tests/Fig/*.pdf Fig/.
	cp rbf-inversions/Fig/*.pdf Fig/.

NR:
	cp FigNR/*.pdf Fig/.

################################################################################

burn:  
	make --directory=rbf-tests burn
	make --directory=rbf-inversions burn

################################################################################
