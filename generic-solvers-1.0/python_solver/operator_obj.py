#!/usr/bin/python
#module containing class for operator
import sep_python as sep


class Operator:
	__operator_list=[]
	
	def __init__(self,op_name,cmd_file,input_file,output_file,input_m0_file=None):
		"""Constructor for Operator object
			input parameters => op_name,cmd_file,input_file,output_file,input_m0_file (necessary for non-linear)
		"""
		self.name=op_name
		self.cmd_file=cmd_file
		self.input=input_file
		self.input_m0=input_m0_file
		self.output=output_file
		self.__command_read()
		Operator.__operator_list.append(self.name)
		return
		
	def run(self,print_cmd=False,print_output=False):
		"""Method to run operator"""
		for cmd_to_run in self.__cmd:
			sep.RunShellCmd(cmd_to_run,print_cmd,print_output)
		stat=0
		return stat
		
	def set_input_output(self,input=None,output=None,input_m0=None):
		"""Method to change name of operator input and/or output"""
		if(input!=None):
			#Changing input file name
			for ii in range(0,len(self.__cmd)):
				self.__cmd[ii]=self.__cmd[ii].replace(self.input,input)
			self.input=input
		if(output!=None):
			#Changing output file name
			for ii in range(0,len(self.__cmd)):
				self.__cmd[ii]=self.__cmd[ii].replace(self.output,output)
			self.output=output
		if((input_m0!=None) and (self.input_m0!=None)):
			#Changing input_m0 file name
			for ii in range(0,len(self.__cmd)):
				self.__cmd[ii]=self.__cmd[ii].replace(self.input_m0,input_m0)
			self.input_m0=input_m0
		elif((input_m0!=None) and (self.input_m0==None)):
			assert False, "Changing input_m0 without inizitializing it"
		return
		
	def print_commands(self):
		"""Method to print commands in the operator"""
		for cmd_to_run in self.__cmd:
			print cmd_to_run
		return
		
	def print_operators(self):
		print "Operators currently instantiated:",Operator.__operator_list
		return
		
	def __del__(self):
		"""Standard destructor"""
		Operator.__operator_list.remove(self.name)
		return
	
	# Read in the command text file
	def __command_read(self):
		self.__cmd=[]
		with open(self.cmd_file) as file:
			for line in file:
				if(line[0:4]=="RUN:"):
					cmd_tmp=line.split("RUN:")[1]
					self.__cmd.append(cmd_tmp.strip('\n'))
		return
		
