#module containing object for linear symmetric stopper (gradient is equal to residual (1/2m'Am -b'm))
import sys
import operator
import math
import sep_python as sep
import python_solver_outcore as solv
from timeit import default_timer as timer





class stopper_symmetric(solv.stopper):
	
	
	def __init__(self,niter=0,maxfevals=0,maxhours=0.0,tolr=1.0e-18,tolobj=None):
		self.niter=niter
		self.maxfevals=maxfevals
		self.maxhours=maxhours
		self.tolr=tolr
		self.tolobj=tolobj
		#Starting timer
		self.__start=timer()
		return
		
		#Beware stopper is going to change the gradient/obj/res files
	def run(self,iter,prblm,log_file=None):
		#Variable to impose stopping to solver
		stop = False
		#Taking time run so far (hours)
		elapsed_time=(timer()-self.__start)/3600.0
		solv.write_log_file(log_file,info="Minutes elapsed: %s"%(elapsed_time*60.0))
		solv.write_log_file(log_file,info="Hours   elapsed: %s"%(elapsed_time))
		#Stop by number of iterations
		if((self.niter > 0) and (iter > self.niter)):
			stop = True
			info =  "Terminate: maximum number of iterations reached"
			print info
			solv.write_log_file(log_file,info=info)
			return stop
		if((self.maxfevals > 0) and (prblm.get_fevals() >= self.maxfevals)):
			stop = True
			info =  "Terminate: maximum number of evaluations"
			print info
			solv.write_log_file(log_file,info=info)
			return stop
		if((self.maxhours > 0.) and (elapsed_time >= self.maxhours)):
			stop = True
			info =  "Terminate: maximum number hours reached %s"%(elapsed_time)
			print info
			solv.write_log_file(log_file,info=info)
			return stop
		if(prblm.get_rnorm() < self.tolr):
			stop = True
			info =  "Terminate: minimum residual tolerance reached %s"%(prblm.get_rnorm())
			print info
			solv.write_log_file(log_file,info=info)
			return stop
		if(self.tolobj!=None):
			if(prblm.get_obj_value() < self.tolobj):
				stop = True
				info =  "Terminate: objective function value tolerance of %s reached, objective function value %s"%(self.tolobj,prblm.get_obj_value())
				print info
				solv.write_log_file(log_file,info=info)
				return stop
		return stop
		
		
		
		
