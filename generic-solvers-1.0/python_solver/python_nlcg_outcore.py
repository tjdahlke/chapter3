#module containing object for non-linear conjugate gradient solver
import sys
import operator
import math
import sep_python as sep
import python_solver_outcore as solv
import stepper_sample as simp_stpr
import stopper_basic as simp_stop


#Beta functions
#grad=new gradient, grad0=old, dir=search direction
#From A SURVEY OF NONLINEAR CONJUGATE GRADIENT METHODS

def errhandler():
	print "Beta function not recognized"
	return

def betaFR(grad,grad0,dir):
	"""Fletcher and Reeves method"""
	#betaFR = sum(dprod(g,g))/sum(dprod(g0,g0))
	dot_grad=sep.Dot_incore(grad,grad)
	dot_grad0=sep.Dot_incore(grad0,grad0)
	if (dot_grad0 == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_grad/dot_grad0
	return beta
	
def betaPRP(grad,grad0,dir):
	"""Polak, Ribiere, Polyak method"""
	#betaPRP = sum(dprod(g,g-g0))/sum(dprod(g0,g0))
	tmp1=sep.tmp_file("tmp1betaPRP.H")
	sep.Cp(grad,tmp1)
	#g-g0
	sep.Sum(tmp1,grad0,1.0,-1.0)
	dot_num=sep.Dot_incore(grad,tmp1)
	dot_grad0=sep.Dot_incore(grad0,grad0)
	if (dot_grad0 == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_num/dot_grad0
	sep.Rm(tmp1);
	return beta
	
def betaHS(grad,grad0,dir):
	"""Hestenes and Stiefel"""
	#betaHS = sum(dprod(g,g-g0))/sum(dprod(d,g-g0))
	tmp1=sep.tmp_file("tmp1betaPRP.H")
	sep.Cp(grad,tmp1)
	#g-g0
	sep.Sum(tmp1,grad0,1.0,-1.0)
	dot_num=sep.Dot_incore(grad,tmp1)
	dot_denom=sep.Dot_incore(dir,tmp1)
	if (dot_denom == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_num/dot_denom
	sep.Rm(tmp1);
	return beta	
	
def betaCD(grad,grad0,dir):
	"""Conjugate Descent"""
	#betaCD = -sum(dprod(g,g))/sum(dprod(d,g0))
	dot_num=sep.Dot_incore(grad,grad)
	dot_denom=sep.Dot_incore(dir,grad0)
	if (dot_denom == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_num/dot_denom
	return beta	
	
def betaLS(grad,grad0,dir):
	"""Liu and Storey"""
	#betaLS = -sum(dprod(g,g-g0))/sum(dprod(d,g0))
	tmp1=sep.tmp_file("tmp1betaLS.H")
	sep.Cp(grad,tmp1)
	#g-g0
	sep.Sum(tmp1,grad0,1.0,-1.0)
	dot_num=sep.Dot_incore(grad,tmp1)
	dot_denom=sep.Dot_incore(dir,grad0)
	if (dot_denom == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_num/dot_denom
	sep.Rm(tmp1);
	return beta		
	
	
def betaDY(grad,grad0,dir):
	"""Dai and Yuan"""
	#betaDY = sum(dprod(g,g))/sum(dprod(d,g-g0))
	tmp1=sep.tmp_file("tmp1betaDY.H")
	sep.Cp(grad,tmp1)
	#g-g0
	sep.Sum(tmp1,grad0,1.0,-1.0)
	dot_num=sep.Dot_incore(grad,grad)
	dot_denom=sep.Dot_incore(dir,tmp1)
	if (dot_denom == 0.): #Avoid division by zero
		beta = 0.
	else:
		beta = dot_num/dot_denom
	sep.Rm(tmp1);
	return beta	

  
def betaHZ(grad,grad0,dir):
	"""Hager and Zhang"""
	#betaN = sum(dprod(g-g0-2*sum(dprod(g-g0,g-g0))*d/sum(dprod(d,g-g0)),g))/sum(dprod(d,g-g0))
	tmp1=sep.tmp_file("tmp1betaHZ.H")
	sep.Cp(grad,tmp1)
	#g-g0
	sep.Sum(tmp1,grad0,1.0,-1.0)
	#sum(dprod(g-g0,g-g0))
	dot_diff_g_g0=sep.Dot_incore(tmp1,tmp1)
	#sum(dprod(d,g-g0))
	dot_dir_diff_g_g0=sep.Dot_incore(dir,tmp1)
	if (dot_dir_diff_g_g0 == 0.): #Avoid division by zero
		beta = 0.
	else:
		#g-g0-2*sum(dprod(g-g0,g-g0))*d/sum(dprod(d,g-g0))
		sep.Sum(tmp1,dir,1.0,-2.0*dot_diff_g_g0/dot_dir_diff_g_g0)
		#sum(dprod(g-g0-2*sum(dprod(g-g0,g-g0))*d/sum(dprod(d,g-g0)),g))
		dot_num=sep.Dot_incore(tmp1,grad)
		#dot_num/sum(dprod(d,g-g0))
		beta = dot_num/dot_dir_diff_g_g0
	sep.Rm(tmp1);
	return beta	  
	
	

class nlcg_solver(solv.solver):
	#Dictionary containing beta functions
	betafunction = {
	"FR" : betaFR,
	"PRP": betaPRP,
	"HS" : betaHS,
	"CD" : betaHS,
	"LS" : betaLS,
	"DY" : betaDY,
	"HZ" : betaHZ
	}

	def __init__(self,stoppr=simp_stop.stopper_basic(),beta_type="FR"):
		"""Constructor for non-linear conjugate gradient solver"""
		#Defining stepper and stopper objects
		self.stoppr=stoppr
		self.beta_type=beta_type
		self.files_to_clean=[]
		self.removefiles=True
		return
		
	def __del__(self):
		"""Overriding default destructor"""
		import sep_python as sep
		if (self.removefiles):
			sep.Rm(self.files_to_clean)
		else:
			for ifile in self.files_to_clean:
				print "	Temporary file not removed for debugging in NON-LINEAR CG: %s"%(ifile)
		return
		
		
	def run(self,prblm,stpr=simp_stpr.stepper_sample(),log_file=None):
		"""Running the solver"""
		#Writing first line in log file if present
		if(not prblm.restart.restarting): solv.write_log_file(log_file,info="NON-LINEAR CONJUGATE GRADIENT SOLVER log file")
		if(not prblm.restart.restarting): solv.write_log_file(log_file,info="Restarting folder: %s\n"%(prblm.restart.restart_folder))
		if(not prblm.restart.restarting): solv.write_log_file(log_file,info="Conjugate method used: %s \n"%(self.beta_type))
		if(not prblm.restart.restarting and hasattr(prblm, 'epsilon')): 
			info = "Regularized inversion with epsilon value of: %s\n"%(prblm.epsilon)
			print info
			solv.write_log_file(log_file,info)
		#Defining beta function computation
		beta_fun=nlcg_solver.betafunction.get(self.beta_type,errhandler)
		#Set internal previous gradient file
		grad0=sep.tmp_file("grad0_nlcg.H");	self.files_to_clean.append(grad0)
		dmodl=sep.tmp_file("dmodel_nlcg.H");self.files_to_clean.append(dmodl)
		modl=sep.tmp_file("model_nlcg.H");	self.files_to_clean.append(modl)
		beta = 0.0
		iter = 1
		#Set internal previous gradient file
		modl_prblm=prblm.get_model()
		sep.Cp(modl_prblm,grad0)
		sep.Zero(grad0)
		#Set internal search direction file
		sep.Cp(modl_prblm,dmodl)
		sep.Zero(dmodl)
		#Set internal model file
		sep.Cp(modl_prblm,modl)
		if(not prblm.restart.restarting):
			obj=prblm.get_obj(modl)
			prblm.initial_obj_value=sep.Get_value(obj) #For relative objective function value
			info = "iter = %s obj = %s residual norm = %s gradient norm= %s feval = %s"%(iter-1,sep.Get_value(obj),prblm.get_rnorm(),prblm.get_gnorm(),prblm.get_fevals())
			print info
			#Writing on log file
			solv.write_log_file(log_file,info=info)
			#Creating restart folder
			prblm.restart.create_restart_folder()
			solv.write_log_file(prblm.restart.log_file,info="NON-LINEAR CONJUGATE GRADIENT SOLVER restart log file\n")
			solv.write_log_file(prblm.restart.log_file,info="\n"+info)
		while True:
			#Restart inversion from previous run
			if(prblm.restart.restarting):
				#Restarting the problem and getting iteration number
				iter=prblm.restart.set_restart(prblm,log_file)+1
				info="Restarting inversion from previous run from: %s"%(prblm.restart.restart_folder)
				solv.write_log_file(log_file,info)
				print info
				prblm.restart.copy_file_from_restart("model_restart_NLCG.H",modl)
				prblm.restart.copy_file_from_restart("dmodel_restart_NLCG.H",dmodl)
				prblm.restart.copy_file_from_restart("grad0_restart_NLCG.H",grad0)
				#Resetting last step length
				if hasattr(stpr, 'alpha'):
					stpr.alpha=float(prblm.restart.get_info_log("step_length"))
				prblm.initial_obj_value = float(prblm.restart.get_info_log("obj",iter_num=0))
				prblm.restart.restarting=False
				
			#Compute problem gradient
			grad=prblm.get_grad(modl)
			if(prblm.get_gnorm() == 0.): 
				print "Gradient vanishes identically"
				break
			prblm.output(modl)
			if(iter > 1):  
				beta = beta_fun(grad, grad0, dmodl)
				#grad0=grad
				sep.Cp(grad,grad0)
			if(beta < 0.): beta = 0.
			#Writing on log file
			solv.write_log_file(log_file,info="beta coefficient: %s"%(beta))
			#dmodl = beta*dmodl - grad
			sep.Sum(dmodl,grad,beta,-1.0)
			#Calling line search (for some reason cannot be put in the solver_obj)
			alpha,success=stpr.run(prblm,modl,dmodl,log_file)
			if(not success): 
				info = "Stepper couldn't find a proper step size, will terminate solver"
				print info
				solv.write_log_file(log_file,info=info)
				break
			obj=prblm.get_obj(modl)
			#Saving current model, previous search direction and gradient in case of restart
			prblm.restart.write_file(modl,"model_restart_NLCG.H")
			prblm.restart.write_file(dmodl,"dmodel_restart_NLCG.H")
			prblm.restart.write_file(grad0,"grad0_restart_NLCG.H")
			info = "iter = %s obj = %s residual norm = %s gradient norm= %s feval = %s"%(iter,sep.Get_value(obj),prblm.get_rnorm(),prblm.get_gnorm(),prblm.get_fevals())
			print info
			#Writing on log file
			solv.write_log_file(log_file,info="\n"+info)
			solv.write_log_file(prblm.restart.log_file,info="step_length = %s"%(alpha))
			solv.write_log_file(prblm.restart.log_file,info="\n"+info)
			iter = iter + 1
			#Beware stopper is going to change the gradient/obj/res files
			if (self.stoppr.run(iter,prblm,log_file)): break
		
		#Writing last inverted model
		prblm.output(modl)
		#Solver has finished, no need for restart folder
		prblm.restart.clean_problem_restart()
		#Writing on log file
		solv.write_log_file(log_file,info="Solver will terminate\n")
		solv.write_log_file(log_file,info="NON-LINEAR CONJUGATE GRADIENT SOLVER log file end")
		return
		
