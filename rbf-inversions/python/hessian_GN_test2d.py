#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
from GNhess2d import *

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class make_hessGN(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out

        # Read in the job parameters
        self.param_reader=param_reader
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.vel_path = self.dict_args['vel_path']
        self.genpar = self.dict_args['genpar']
        self.masksalt = self.dict_args['masksalt']
        self.scaling = self.dict_args['scaling']
        self.prefix = self.dict_args['prefix']

        self.pad = int(self.dict_args['pad'])
        self.randpad = int(self.dict_args['randpad'])

        self.nz = int(self.dict_args['nz'])
        self.ny = int(self.dict_args['ny'])

        self.nzr = int(self.dict_args['nzr'])
        self.nyr = int(self.dict_args['nyr'])

        self.fz = int(self.dict_args['fz'])+self.randpad
        self.fy = int(self.dict_args['fy'])+self.randpad

        self.gnhess_obj=make_GNHESS(param_reader)

        hessIN = "%s/hessGNin_full.H" % (self.path_out)
        hessOUT = "%s/hessGNout_full.H" % (self.path_out)

        #----- MASKING AND SCALING FULL MODEL -----------------------
        maskop = "Math file1=%s file2=%s exp='file1*file2' > %s" % (
            self.masksalt, self.hess_in_path, hessIN)
        print(maskop)
        subprocess.call(maskop, shell=True)

        #----- RUN PBS:  (B^T)B --------------------------------------------
        hesstag = 'HessGN-%s' % (self.prefix)
        cleanup=True
        self.gnhess_obj.GNHESS_run(self.param_reader,hesstag,cleanup,True,hessIN,hessOUT)

        #----- MASKING AND SCALING FULL MODEL -----------------------
        maskop = "Math file1=%s file2=%s exp='file1*file2' > %s;" % (
            self.masksalt, hessOUT, self.hess_out_path)
        subprocess.call(maskop, shell=True)


        return




if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessGN(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)
