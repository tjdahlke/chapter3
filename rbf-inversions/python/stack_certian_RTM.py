#================================================================================================
#       Program to grab RTM images and stack them.
#
#       Usage:
#               >> python find_all_node_pos.py input=data_header.H output=gridfile param=paramfile.p
#
#		paramfile.p contains:
#			pad: absorbing boundary padding for vel model
#			dz,dx,dy: spatial sampling in vel model
#			oz,ox,oy: origin of the REGION OF INTEREST, NOT THE PADDED MODEL
#
#       Taylor Dahlke, 6/1/2018 taylor@sep.stanford.edu
#================================================================================================

#!/usr/bin/python
import sys
import subprocess
import math, copy
import sep2npy
import numpy as np




# Read in the input parameters
eq_args_from_cmdline, args = sep2npy.parse_args(sys.argv)
pars=sep2npy.RetrieveAllEqArgs(eq_args_from_cmdline)
outfile=pars["outfile"]
infile=pars["infile"]
prefix=pars["prefix"]
xmin=float(pars["xmin"])
xmax=float(pars["xmax"])
ymin=float(pars["ymin"])
ymax=float(pars["ymax"])

# Clear out the old file so as not to simply append to it
cmd = "rm %s" % (outfile)

# Read in the binary represented by the infile file
input2d=sep2npy.sep_read_scratch(infile)

# Get dimensions of infile
nnodes=int(sep2npy.get_sep_his_par(infile, 'n2'))

# Read in a node position from list
count=0
for ii in range(0,nnodes):
	xx=input2d[ii,0]
	yy=input2d[ii,1]
	zz=input2d[ii,2]
	print("----------------------------------------------------")
	print("%s | %s | %s" % (xmin,xx,xmax))
	print("%s | %s | %s" % (ymin,yy,ymax))
	# Check if node position is smaller than value
	if (ymin < yy < ymax):
		if (xmin < xx < xmax):
			# If so, them add to stack
			index=ii+1
			if (count==0):
				cmd="Cp %s-%d.H  %s" % (prefix,index,outfile)
			else:
				cmd="Add %s %s-%d.H > tmp.h; Cp tmp.h %s" % (outfile,prefix,index,outfile)
			print(cmd)
			subprocess.call(cmd,shell=True)
			count=count+1



