

# Make RTM images based on offset and angular range from the inclusion
# Taylor Dahlke 7/13/2018


import sepbase
import sep2npy
import sys,os,math
import commands


# Function to check node position
def check_node(nodeAngle,nodeOffset,minAng,maxAng,minOff,maxOff):
	if ( (minOff<nodeOffset<maxOff) and (minAng<nodeAngle<maxAng) ):
		goodnode=True
	else:
		goodnode=False
	return goodnode




# MAIN PROGRAM
if __name__ == '__main__':
	eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
	dict_args = eq_args_from_cmdline

	################  PARAMETERS ################
	# List of nodes and relative positions
	recpos = dict_args['recpos']
	# X coordinate of inclusion
	xpos = float(dict_args['xpos'])
	# Y coordinate of inclusion
	ypos = float(dict_args['ypos'])
	# Number of angle bins
	numAng = int(dict_args['numAng'])
	# Number of offset bins
	numOff = int(dict_args['numOff'])
	# File path/prefix
	rtmpath = dict_args['rtmpath']
	# Max offset to consider
	maxOff = float(dict_args['maxOff'])
	# Model dx
	dx = float(dict_args['dx'])
	# Model dy
	dy = float(dict_args['dy'])
	# Model ox
	ox = float(dict_args['ox'])
	# Model oy
	oy = float(dict_args['oy'])
	# PBS template
	PBStemp = dict_args['PBStemp']
	# Output path for PBS stuff
	outpath = dict_args['outpath']
	# Datapath
	datapath = dict_args['datapath']
	# Python path
	pypath = dict_args['pypath']
	# Binary path
	binpath = dict_args['binpath']

	# Initialize
	minAng=0.0
	dAng=360.0/(numAng)
	dOff=maxOff/numOff
	reclist=[]
	pbsjobnum=0
	cmd1="less %s | wc -l" % (recpos)
	stat1,numNodes=commands.getstatusoutput(cmd1)
	recList = list(open(recpos))
	numNodesFound=0

	# Loop over angles
	for ang in range(0,numAng):
		minAng=minAng
		maxAng=minAng+dAng
		minOff=0.0
		
		# Loop over offsets
		for off in range(0,numOff):
			maxOff=minOff+dOff
			validNodes=" "
			nodeFound=False

			# Loop over nodes
			for ii in range(0,int(numNodes)):

				# Grab node info if new node
				nodeId=int(str(recList[ii]).split()[0])
				nodeZ=int(str(recList[ii]).split()[1])
				nodeY=int(str(recList[ii]).split()[2])*dy+oy
				nodeX=int(str(recList[ii]).split()[3])*dx+ox

				# Find distance of node from inclusion
				diffX=(nodeX-xpos)
				diffY=(nodeY-ypos)
				nodeOffset=math.sqrt(diffX**2+diffY**2)

				# Find angle of node from inline direction (line along constant: ~x=214800)
				nodeAngle=math.degrees(math.atan(diffX/diffY))
				if ((diffX>0) and (diffY<0)): # Q2 LR
					nodeAngle=nodeAngle+180
				if ((diffX<0) and (diffY<0)): # Q4 LL
					nodeAngle=nodeAngle+180
				if ((diffX<0) and (diffY>0)): # Q3 UL
					nodeAngle=nodeAngle+360

				# Check node off and ang
				aa=check_node(nodeAngle,nodeOffset,minAng,maxAng,minOff,maxOff)
				if (aa):
					nodeFound=True
					RTMfile="%s%d.H" % (rtmpath,nodeId)
					bb=sepbase.CheckSephFileError(RTMfile)
					if (not bb):
						validNodes="%s %s" % (validNodes,RTMfile)
						numNodesFound=numNodesFound+1

			print("# of nodes found:   %s" %(numNodesFound))
			# Stack the files that were selected, if any
			if (nodeFound):
				pbsjobnum=pbsjobnum+1
				cmd0 = "setenv DATAPATH %s" % (datapath)
				stackfile="%s/RTM_o.%d-o.%d-a.%f-a.%f.H" % (outpath,minOff,maxOff,minAng,maxAng)
				if (sepbase.CheckSephFileError(stackfile)==0):
					print("Made this stacked file already\n")
				else:
					cmd1='python %s/recursive_add.py %s %s "%s" ' % (pypath,binpath,stackfile,validNodes)
					# cmd1="Add %s > %s" % (validNodes,stackfile)
					print(cmd1)
					# Make a qsub file
					pbsfile="%s/PBSrtmStack%d.sh" % (outpath,pbsjobnum)
					cmd2="cp %s %s; echo '%s' >> %s; echo '%s' >> %s  " % (PBStemp,pbsfile,cmd0,pbsfile,cmd1,pbsfile)
					stat1,junk1=commands.getstatusoutput(cmd2)
					# Submit the qsub file
					cmd3="qsub -o %s.O -e %s.E %s" % (pbsfile,pbsfile,pbsfile)
					print(cmd3)
					stat2,junk2=commands.getstatusoutput(cmd3)

			# Check next offset bin
			minOff=maxOff

		# Check next angle bin
		minAng=maxAng


# print("=======================================")
# print("minOff: %s  Off: %s  maxOff: %s" % (minOff,nodeOffset,maxOff))
# print("minAng: %s  Ang: %s  maxAng: %s" % (minAng,nodeAngle,maxAng))



