#================================================================================================
#       Program to make salt boundary mask
#
#       Usage:
#               >> python MAKE_MASK.py input=guess_phi.H output=masksalt.H
#
#
#       Taylor Dahlke, 10/22/2018 taylor@sep.stanford.edu
#================================================================================================

#!/usr/bin/python

import subprocess
import sepbasic
import sys
import math, copy

# Read in the input parameters.
eq_args_from_cmdline, args = sepbasic.parse_args(sys.argv)
pars=sepbasic.RetrieveAllEqArgs(eq_args_from_cmdline)
masksalt=pars["masksalt"]
infile=pars["infile"]
width=pars["width"]
binpath=pars["binpath"]
guide=pars["guide"]


# Get the norm of the gradient of phi (the phi slope steepness).
cmd1="%s/PHI_SLOPE.x verbose=1 < %s | Scale > %s" % (binpath,infile,masksalt)
# add="Scale < %sp > %sp2; Math file1=%sp2 exp='file1+0.02' | Scale > %s " % (masksalt,masksalt,masksalt,masksalt)
# cmdALL="%s %s;  " % (cmd1,add)
# print("\n\n %s" % (cmdALL))
subprocess.call(cmd1,shell=True)


# # Get the norm of the gradient of phi (the phi slope steepness).
# cmd0a="%s/CLIP.x clipval=0.0 replacelessthan=0 replaceval=4500.0 < %s > %s0a" % (binpath,infile,masksalt)
# cmd0b="%s/CLIP.x clipval=4400.0 replaceval=2000.0 < %s0a | Scale > %s0b" % (binpath,masksalt,masksalt)
# cmd1="%s/PHI_SLOPE.x verbose=1 < %s0b > %s1" % (binpath,masksalt,masksalt)
# cmd2="%s/CLIP.x clipval=0.1 replaceval=0.0 < %s1 | Scale > %s2" % (binpath,masksalt,masksalt)
# # Smooth that slope
# cmd3="Smooth rect1=%s rect2=%s rect3=%s < %s2 | Scale > %s3" % (width,width,width,masksalt,masksalt)
# cmdcommon="%s; \n %s; \n %s; \n %s; \n %s;" % (cmd0a,cmd0b,cmd1,cmd2,cmd3)

# cmd5="%s/CLIP.x clipval=0.3  replacelessthan=0  < %s3 | Scale > %s5" % (binpath,masksalt,masksalt)
# cmd6="Smooth rect1=%s rect2=%s rect3=%s < %s5 | Scale > %swb" % (width,width,width,masksalt,masksalt)
# rmlow="%s/CLIP.x clipval=0.1 replaceval=0.0 < %swb > %sp" % (binpath,masksalt,masksalt)
# add="Scale < %sp > %sp2; Math file1=%sp2 exp='file1+0.02' | Scale > %s " % (masksalt,masksalt,masksalt,masksalt)
# cmdALL="%s %s; %s; %s; %s; " % (cmdcommon,cmd5,cmd6,rmlow,add)
# print("\n\n %s" % (cmdALL))
# subprocess.call(cmdALL,shell=True)
# rmcmd="rm -f %s0a %s0b %s1 %s2 %s3 %s4 %s5 %swb %sp %sp2" % (masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt)
# subprocess.call(rmcmd,shell=True)




