#================================================================================================
#       Program to convert header physical sx,sy,gx,gy, to the index positions on the model grid
#
#       Usage:
#               >> python convert_header.py input=data_header.H output=gridfile param=paramfile.p
#
#		paramfile.p contains:
#			pad: absorbing boundary padding for vel model
#			dz,dx,dy: spatial sampling in vel model
#			oz,ox,oy: origin of the REGION OF INTEREST, NOT THE PADDED MODEL
#
#       Taylor Dahlke, 2/2/2018 taylor@sep.stanford.edu
#================================================================================================

#!/usr/bin/python
import subprocess
import sys
import math, copy
import sep2npy
import numpy as np


# Read in the input parameters
eq_args_from_cmdline, args = sep2npy.parse_args(sys.argv)
pars=sep2npy.RetrieveAllEqArgs(eq_args_from_cmdline)
outfile=pars["outfile"]
infile=pars["infile"]

# Clear out the old file so as not to simply append to it
cmd = "rm %s" % (outfile)

# Read in the binary represented by the infile file
input2d=sep2npy.sep_read_scratch(infile)

# Get dimensions of infile
nkeys=int(sep2npy.get_sep_his_par(infile, 'n1'))
ntraces=int(sep2npy.get_sep_his_par(infile, 'n2'))

# Get the key number for each named entry
for ik in range(1,nkeys+1):
	parname="hdrkey%d" % (ik)
	val=sep2npy.get_sep_his_par(infile, parname)
	if (val=="nodeId"):
		Knodeid=ik-1
	if (val=="GZ"):
		Kgz=ik-1
	if (val=="GX"):
		Kgx=ik-1
	if (val=="GY"):
		Kgy=ik-1

input1d=input2d[:,Knodeid]


# Make list
reclist=[]
nodeIdList=[]
nodeInfo=[]
# Search for nodeid's
oldNodeId=0
for ii in range(0,ntraces):
# for ii in range(0,10000):
	nodeId=int(input1d[ii])
	# print("percent searched= %f" % (100.0*ii/ntraces))
	if (nodeId != oldNodeId):

		# Grab node info if new node
		nodeInfo=[ input2d[ii,Knodeid],input2d[ii,Kgz],input2d[ii,Kgy],input2d[ii,Kgx] ]
		# print(nodeInfo)
		reclist.append(nodeInfo)
		nodeInfo=[]
		nodeIdList.append(nodeId)

		# Search for a new nodeId
		oldNodeId=nodeId

print("=====================================")
print("Check that all nodeid's are unique")
seen = set()
uniq = []
for x in nodeIdList:
    if x not in seen:
        uniq.append(x)
        seen.add(x)

# print(nodeIdList)
# print(reclist)

print("=====================================")
print("Write to output")
for jj in range(0,len(uniq)):
	print("percent written = %f" % (100.0*jj/len(uniq)))
	# Write the reclist info to disk
	# nodeInfo=reclist[jj]
	nodeInfo = "%d %d %d %d" % ( reclist[jj][0],reclist[jj][1],reclist[jj][2],reclist[jj][3])
	cmd = "echo %s >> %s" % (nodeInfo,outfile)
	subprocess.call(cmd,shell=True)


print("=============================================")
print("Length of original list: %d" % len(reclist))
print("Length of final list: %d" % len(uniq))
print("Number of diplicate nodeIds: %d" % (len(reclist)-len(uniq)))


