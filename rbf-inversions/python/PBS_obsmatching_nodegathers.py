# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
recfile = sys.argv[1]
syn_prefix = sys.argv[2]
obs_prefix = sys.argv[3]
writepath = sys.argv[4]
pbsTemplate = sys.argv[5]
pythonpath = sys.argv[6]
path_out = sys.argv[7]

cmd1="less %s | wc -l" % (recfile)
stat1,numNodes=commands.getstatusoutput(cmd1)
recList = list(open(recfile))


# Visit each location to grab the binned shot data
for ii in range(1,int(numNodes)):
# for ii in range(0,10):

    print("----------------------------------------")
    percent=100.0*ii/float(numNodes)
    percentcmd="percent done:  %f " % (percent)
    print(percentcmd)

    nodeId=int(str(recList[ii]).split()[0])
    outputFileName="%s/modifiedOBS_%d.H" % (writepath,nodeId)
    temp2="%s/temp2_%d.sh" % (writepath,nodeId)
    finalPBS="%s/PBSscript_%d.sh" % (writepath,nodeId)

    cmdp1="echo '#PBS -o %s/logfile-%s.txt' > %s" % (writepath,nodeId,temp2)
    cmdp2="echo '#PBS -e %s/logfile-%s.txt' >> %s" % (writepath,nodeId,temp2)
    cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,temp2)
    cmdp="%s; %s; %s;" % (cmdp1, cmdp2, cmdp3)
    print(cmdp)
    subprocess.call(cmdp,shell=True)


    # Run the job for a single node
    cmd="python %s/nodegather_work_SINGLE.py %d %s %s %s %s %s" % (pythonpath,nodeId,syn_prefix,obs_prefix,writepath,outputFileName,path_out)
    cmdW1="echo '%s' >> %s;" % (cmd,temp2)
    print(cmdW1)
    subprocess.call(cmdW1,shell=True)

    cmdW3="cat %s %s > %s;" % (pbsTemplate,temp2,finalPBS)
    print(cmdW3)
    subprocess.call(cmdW3,shell=True)
    cmdW4="qsub -l nodes=1:ppn=24 -d /data/biondo/tjdahlke/research/3dtest/outputs/ %s;" % (finalPBS)
    print(cmdW4)
    subprocess.call(cmdW4,shell=True)





