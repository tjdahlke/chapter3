# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
nodeId = int(sys.argv[1])
syn_prefix = sys.argv[2]
obs_prefix = sys.argv[3]
writepath = sys.argv[4]
outputFileName = sys.argv[5]
path_out = sys.argv[6]
smallNum=0.00001


print("----------------------------------------")
filtname="%s/filter_%d.H" % (writepath,nodeId)
synNode="%s/synNode_%d.H" % (writepath,nodeId)
obsNode="%s/obsNode_%d.H" % (writepath,nodeId)

# Grab syn data for node
cmdW="Cp %s/%s_%s %s" % (path_out,syn_prefix,nodeId,synNode)
print(cmdW)
commands.getstatusoutput(cmdW)

# Grab obs data for node
cmdW="Window_key synch=1 key1='nodeId' mink1=%d maxk1=%d < %s > %s hff=%s@@" % (nodeId,nodeId,obs_prefix,obsNode,obsNode)
print(cmdW)
commands.getstatusoutput(cmdW)

ntraces=int(sepbase.get_sep_his_par(synNode,'n2'))
nt=int(sepbase.get_sep_his_par(synNode,'n1'))
# stat1,ntraces=commands.getstatusoutput(cmd1)

# Make filter
f3="< %s Rtoc | Pad n1out=0 | Ft3d sign1=-1 center1=0 > %s/syn_fft%d.H" % (synNode,writepath,nodeId)
f4="< %s Rtoc | Pad n1out=0 | Ft3d sign1=-1 center1=0 > %s/obs_fft%d.H" % (obsNode,writepath,nodeId)
f5="Math datapath=%s file1=%s/syn_fft%d.H file2=%s/obs_fft%d.H exp_real='(file1.re*file2.re+file1.im*file2.im)' exp_imag='(file1.im*file2.re-file1.re*file2.im)' | Stack3d maxsize=10000 > %s/junk_numerator%d.H" % (writepath,writepath,nodeId,writepath,nodeId,writepath,nodeId)
f6="Math datapath=%s file1=%s/obs_fft%d.H file2=%s/obs_fft%d.H exp_real='(file2.re*file2.re+file2.im*file2.im+%f)' exp_imag='file2.im*0.0' | Stack3d maxsize=10000 > %s/junk_denom%d.H"  % (writepath,writepath,nodeId,writepath,nodeId,smallNum,writepath,nodeId)
f7="Math file1=%s/junk_numerator%d.H file2=%s/junk_denom%d.H exp_real='file1.re/file2.re' exp_imag='file1.im/file2.re' | Pad extend=1 n2=%s > %s" % (writepath,nodeId,writepath,nodeId,ntraces,filtname)
# f8="Rm junk_*.H"
cmdF="%s; %s; %s; %s; %s" % (f3,f4,f5,f6,f7)
print(cmdF)
# commands.getstatusoutput(cmdF)
subprocess.call(cmdF,shell=True)


# Apply filter
cmdA="Math datapath=%s file1=%s/obs_fft%d.H file2=%s exp_real='file1.re*file2.re-file1.im*file2.im' exp_imag='file1.re*file2.im+file1.im*file2.re' | Ft3d sign1=1 center1=0 | Real | Window3d n1=%d hff=%s@@ > %s" % (writepath,writepath,nodeId,filtname,nt,outputFileName,outputFileName)
# commands.getstatusoutput(cmdA)
subprocess.call(cmdA,shell=True)






