# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
recfile = sys.argv[1]
genpar = sys.argv[2]
inputvel = sys.argv[3]
writepath = sys.argv[4]
pbsTemplate = sys.argv[5]
pythonpath = sys.argv[6]
binpath = sys.argv[7]
pad = sys.argv[8]
randpad = sys.argv[9]

print("\n\n")
print(recfile)
cmd1="less %s | wc -l" % (recfile)
stat1,numNodes=commands.getstatusoutput(cmd1)
recList = list(open(recfile))



# Visit each location to grab the binned shot data
for ii in range(0,int(numNodes)):
# for ii in range(0,10):


    print("----------------------------------------")
    percent=100.0*ii/float(numNodes)
    percentcmd="percent done:  %f " % (percent)
    print(percentcmd)

    nodeId=int(str(recList[ii]).split()[0])
    outputFileName="%s/randomABC_%d.H" % (writepath,nodeId)
    temp2="%s/temp2_%d.sh" % (writepath,nodeId)
    finalPBS="%s/PBSscript_%d.sh" % (writepath,nodeId)


    # Check if file already exists
    check="ls %s" % (outputFileName)
    stat2,junk=commands.getstatusoutput(check)
    if (stat2>0):

        cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,temp2)
        cmdp4="echo '#PBS -d %s' >> %s" % (writepath,temp2)
        cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (writepath,temp2)
        cmdp="%s; %s; %s;" % (cmdp3, cmdp4, cmdp5)
        print(cmdp)
        subprocess.call(cmdp,shell=True)

        # Run the job for a single node
        cmd="python %s/random_ABC_SINGLE2D.py %d %s %s %s %s %s %s %s" % (pythonpath,nodeId,pad,binpath,genpar,inputvel,outputFileName,writepath,randpad)
        cmdW1="echo '%s' >> %s;" % (cmd,temp2)
        print(cmdW1)
        subprocess.call(cmdW1,shell=True)

        cmdW3="cat %s %s > %s;" % (pbsTemplate,temp2,finalPBS)
        print(cmdW3)
        subprocess.call(cmdW3,shell=True)
        cdmS="sleep 1"
        print(cdmS)
        subprocess.call(cdmS,shell=True)
        cmdW4="qsub -l nodes=1:ppn=1 -d %s %s;" % (writepath,finalPBS)
        print(cmdW4)
        subprocess.call(cmdW4,shell=True)





