program MUTE

  ! Program for muting direct arrival from data using cos^2 muting transition
  !
  ! Inputs:
  !   1) history file of data
  !
  ! Outputs:
  !   1) history file of muted data
  !
  ! Taylor Dahlke, March 2018
  !
  ! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
  !     NOTE: USE IFORT COMPILE FLAG: -heap-arrays SO THAT THE RESHAPE FUNCTION DOESNT FAIL FOR LARGE ARRAYS
  ! VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV


  !=================================
  ! standard declarations
  !=================================
  use sep
  implicit none


  !=================================
  ! declare variables
  !=================================
  ! program variables
  integer                           :: ii,it,it1c,it2c,it1,it2,ntraces,nt,muteLag,nKeys
  real                              :: ot,dt,muteVel,maxOff,maxTime,tt,t0,dz,pi,timeShift
  real, dimension(:),allocatable    :: output,depth,offset,cos2Func,muteMask
  real, dimension(:,:),allocatable  :: header
  call sep_init()

  ! history file parameters
  call from_history('n1',nt)
  call from_history('n2',ntraces)
  call from_history('o1',ot)
  call from_history('d1',dt)

  ! other parameters
  call from_param('muteVel',muteVel,1500.0)
  call from_param('muteLag',muteLag,100)
  call from_param('timeShift',timeShift,0.5)
  call from_param('maxOff',maxOff,1000000000000.0)
  call from_param('dz',dz,12.5)
  call from_aux("header","n1",nKeys)
  maxTime=nt*dt+ot
  pi=3.1415927

  ! make arrays
  allocate(header(nKeys,ntraces))
  allocate(depth(ntraces))
  allocate(output(nt))
  allocate(offset(ntraces))
  allocate(muteMask(ntraces))
  allocate(cos2Func(2*muteLag+1))

  call to_history("n1",(2*muteLag+1),'test')
  call to_history("n1",nt,'muteMask')
  call to_history("n2",ntraces,'muteMask')

  ! Read header
  call sep_read(header,"header")
  depth=ABS(header(5,:)-header(2,:))*dz

  offset=header(8,:)

  ! Create the muting taper function 
  ! (Central / symmetric muting around the direct arrival)
  do ii=1,(2*muteLag+1)
      cos2Func(ii)=1.0-(COS((ii-1)*pi/(2*muteLag)-(pi/2))**2)
      cos2Func(ii) = cos2Func(ii)*cos2Func(ii)
  end do

  ! Mute traces
  do ii=1,ntraces
      t0=depth(ii)/muteVel

      write(0,*) "percent done: ", 100*ii/ntraces
      call sep_read(output)
      if (offset(ii)<=maxOff) then ! Ignore the large offsets if maxOff set
          tt = sqrt( (t0*t0) + ((offset(ii)*offset(ii))/(muteVel*muteVel)) ) ! Window according to a hyperbola
          it = 1.5+(timeShift+tt-ot)/dt
          if ((tt+timeShift+(muteLag*dt))>maxTime) then
              it1=it-muteLag
              it2=nt
              it1c=1
              it2c=2*muteLag+1 - (nt-it) !good i think
          elseif ((tt+timeShift-(muteLag*dt))<0.0) then
              it1=1
              it2=it+muteLag
              it1c=muteLag-it+2 !good
              it2c=2*muteLag+1
          else
              it1=it-muteLag
              it2=it+muteLag
              it1c=1
              it2c=2*muteLag+1
          end if
          output(it1:it2)=output(it1:it2)*cos2Func(it1c:it2c)
          ! muteMask=1.0
          ! muteMask(it1:it2)=muteMask(it1:it2)*cos2Func(it1c:it2c)
          ! if (exist_file('muteMask')) then
          !     call srite('muteMask',muteMask,4*nt)
          ! end if
      end if
      call srite('out',output,4*nt)
  end do

  if (exist_file('muteMask')) then
      call srite('test',cos2Func,4*(2*muteLag+1))
  end if

  write(0,*) " DONE!"

end program




