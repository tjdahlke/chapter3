!===============================================================================
!		ADD
!		File1:  	Nz x Nx X Ny
!		File2:  	Nz x Nx X Ny
!		File3:  	Nz x Nx X Ny
!		Output:  	Nz x Nx X Ny
!
!	When I try to use more than 3 input files it seems to get way more expensive
!===============================================================================

program ADD
	use sep
	implicit none

	integer								:: nz1,nx1,ny1,i
	! real,dimension(:),allocatable		:: inputA1,inputB1,inputC1,output1
	real,dimension(:,:),allocatable		:: inputA2,inputB2,inputC2,inputD2,inputE2,output2
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1
	real								:: timer_sum=0.,dz,dx,dy,oz,ox,oy,percent

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose",verbose,.TRUE.)
	if(verbose) write(0,*) "==================== Read in initial parameters"

	call from_aux("file1","n1",nz1)
	call from_aux("file1","n2",nx1)
	call from_aux("file1","n3",ny1)

	call from_aux("file1","d1",dz)
	call from_aux("file1","d2",dx)
	call from_aux("file1","d3",dy)

	call from_aux("file1","o1",oz)
	call from_aux("file1","o2",ox)
	call from_aux("file1","o3",oy)

	if(verbose) write(0,*) "==================== Allocate"


	allocate(inputA2(nz1,nx1))
	allocate(inputB2(nz1,nx1))
	allocate(inputC2(nz1,nx1))
	allocate(output2(nz1,nx1))



	if(verbose) write(0,*) "==================== Write output header"

	call to_history("n1",nz1)
	call to_history("n2",nx1)
	call to_history("n3",ny1)
	call to_history("o1",oz)
	call to_history("o2",ox)
	call to_history("o3",oy)
	call to_history("d1",dz)
	call to_history("d2",dx)
	call to_history("d3",dy)

	if(verbose) write(0,*) "==================== Running "


	if (exist_file('file3')) then
		! Does adding along 2D arrays
		do i=1,ny1
			call sep_read(inputA2,"file1")
			call sep_read(inputB2,"file2")
			call sep_read(inputC2,"file3")
			output2=inputA2+inputB2+inputC2
			call sep_write(output2)
		end do
	else
		! Does adding along 2D arrays
		do i=1,ny1
			call sep_read(inputA2,"file1")
			call sep_read(inputB2,"file2")
			output2=inputA2+inputB2
			call sep_write(output2)
		end do
	endif


	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)",timer_sum/1000
	write(0,*) "DONE!"

end program