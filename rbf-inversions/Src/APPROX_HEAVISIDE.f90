
!===================================================================================
!		Assemble the velocity model from the implicit surface and background vel
!===================================================================================

program APPROX_HEAVISIDE
use sep
implicit none

	integer										:: nz, nx, ny, iz, iy, ix
	real										:: eps,pi,ss,kappa
	logical										:: verbose
	real										:: vsalt
	real, dimension(:,:), allocatable			:: phi2d, Hside2d
	real, dimension(:,:,:), allocatable			:: phi3d, Hside3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("kappa",kappa,0.1)
	call from_history("n1", nz)
	call from_history("n2", nx)
	call from_history("n3", ny,1)
	pi=3.14159265359


	if (ny==1) then
		if(verbose) write(0,*) "========= Detected 2D inputs ==============="
		allocate(phi2d(nz, nx))
		allocate(Hside2d(nz, nx))
		call sep_read(phi2d)
		! eps=kappa*(maxval(phi2d)-minval(phi2d))
		eps=kappa*2
		
		Hside2d=0.0
		WHERE (phi2d >eps) Hside2d=1.0
		do iz=1,nz
			do ix=1,nx
				ss=phi2d(iz,ix)
				if ( (ss<= eps) .AND. ((-1.0*eps)<=ss) ) then
					Hside2d(iz,ix)=0.5*( 1 + (ss/eps) + (1/pi)*sin(pi*ss/eps) )
				end if
			enddo
		enddo

		call sep_write(Hside2d)

	else
		if(verbose) write(0,*) "========= Detected 3D inputs ==============="
		allocate(phi3d(nz, ny, nx))
		allocate(Hside3d(nz, ny, nx))
		call sep_read(phi3d)
		! eps=kappa*(maxval(phi3d)-minval(phi3d))
		eps=kappa*2
				
		Hside3d=0.0
		WHERE (phi3d >eps) Hside3d=1.0
		do iz=1,nz
			do iy=1,ny
				do ix=1,nx
					ss=phi3d(iz,iy,ix)
					if ( (ss<= eps) .AND. ((-1.0*eps)<=ss) ) then
						Hside3d(iz,iy,ix)=0.5*( 1 + (ss/eps) + (1/pi)*sin(pi*ss/eps) )
					end if
				enddo
			enddo
		enddo

		call sep_write(Hside3d)
	end if

end program
