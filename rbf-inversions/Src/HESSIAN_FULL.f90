
! >>>>>   CAN ONLY TAKE SINGLE SUBSURFACE OFFSET REFLECTION MODELS

PROGRAM FULL_HESSIAN

  USE sep
  USE mem_mod
  USE fbi_mod

  IMPLICIT NONE
  INTEGER                             :: nt, n1, n2, wfld_j, i, nvb, nsb, npc, np
  INTEGER                             :: sou_nx, sou_ox, sou_dx, sou_z, sou_x, isou
  INTEGER                             :: rec_nx, rec_ox, rec_dx, rec_z, rec_x, irec
  INTEGER                             :: bc1, bc2, bc3, bc4, nh, oh
  REAL                                :: dt, d1, o1, d2, o2, eigenshift, val1,val2,val3
  INTEGER                             :: oper
  LOGICAL                             :: adj, verbose
  REAL, DIMENSION(:), ALLOCATABLE     :: wavelet, sparsemodel, sparsedata
  REAL, DIMENSION(:,:), ALLOCATABLE   :: vel, deltaB, input, gnphi, finalvel, modboth
  REAL, DIMENSION(:,:,:), ALLOCATABLE :: source, DATA, deltaD, datatmp, srcwave_in
  INTEGER, DIMENSION(8)               :: timer0, timer1
  REAL                                :: timer_sum=0.

  ! ---------------------------------------------------------------------------
  CALL sep_init()
  CALL DATE_AND_TIME(values=timer0)

  !---------------------------------------------------------------------------
  CALL from_param("isou", isou)
  CALL from_param("verbose", verbose,.FALSE.)
  CALL from_history("n1", n1)
  CALL from_history("o1", o1)
  CALL from_history("d1", d1)
  CALL from_history("n2", n2)
  CALL from_history("o2", o2)
  CALL from_history("d2", d2)
  CALL from_param("nh", nh, 1)
  CALL from_param("oh", oh, 0)
  IF(nh<1-oh) WRITE(0,*) "WARNING: zero subsurface axis is not included"
  CALL from_param("nt", nt)
  CALL from_aux("wavelet", "d1", dt)
  CALL from_param("bc1", bc1)
  CALL from_param("bc2", bc2)
  CALL from_param("bc3", bc3)
  CALL from_param("bc4", bc4)
  CALL from_param("wfld_j", wfld_j)
  CALL from_param("sou_nx", sou_nx)
  CALL from_param("sou_ox", sou_ox)
  CALL from_param("sou_dx", sou_dx)
  CALL from_param("sou_z", sou_z)
  IF(sou_ox < 1 .OR. (sou_nx-1)*sou_dx+sou_ox > n2) CALL erexit("sou axis values are not correct")
  IF(sou_z < 1 .OR. sou_z > n1) CALL erexit("sou_z value is not correct")
  CALL from_param("rec_nx", rec_nx)
  CALL from_param("rec_ox", rec_ox)
  CALL from_param("rec_dx", rec_dx)
  CALL from_param("rec_z", rec_z)
  IF(rec_ox < 1 .OR. (rec_nx-1)*rec_dx+rec_ox > n2) CALL erexit("rec axis values are not correct")
  IF(rec_z < 1 .OR. rec_z > n1) CALL erexit("rec_z value is not correct")

  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Initializing 1,2,3 <<<<<<<<<<<<<<<<<"
  CALL init1(n1, o1, d1, n2, o2, d2, bc1, bc2, bc3, bc4)
  CALL init2(sou_nx, sou_ox, sou_dx, sou_z, rec_nx, rec_ox, rec_dx, rec_z)
  CALL init3(nt, dt, wfld_j, nh, oh)

  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Prepping wavelet and vel <<<<<<<<<<<"
  CALL mem_alloc1d(wavelet, nt)
  CALL mem_alloc2d(vel, n1, n2)
  CALL sep_read(vel,"velmod")
  CALL sep_read(wavelet, "wavelet")
  CALL init_alloc(wavelet)
  CALL model_update(vel)

  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Prep for GN comp  <<<<<<<<<<<<<<<<<<"
  CALL mem_alloc2d(gnphi, n1, n2)
  CALL mem_alloc2d(finalvel, n1, n2)
  CALL mem_alloc3d(datatmp, nt, rec_nx, 1)
  ALLOCATE(modboth(n1,n2))

  ! Read in WEMVA --------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Prep for WEMVA comp <<<<<<<<<<<<<<<<"
  CALL mem_alloc3d(DATA, nt, rec_nx, 1)
  CALL mem_alloc2d(deltaB, n1, n2)
  CALL sep_read(DATA(:,:,1), "data")
  CALL data_update(DATA)

  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Writing history files <<<<<<<<<<<<<<"
  CALL to_history("n1", n1)
  CALL to_history("o1", o1)
  CALL to_history("d1", d1)
  CALL to_history("n2", n2)
  CALL to_history("o2", o2)
  CALL to_history("d2", d2)

  deltaB=0.0

  !-----------------------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Read input <<<<<<<<<<<<<<<<<<<<<<<<<"
  CALL sep_read(modboth)

  !-----------------------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Read srcwave <<<<<<<<<<<<<<<<<<<<<<<"
  IF (exist_file('srcwave_in')) THEN
     ALLOCATE(srcwave_in(n1,n2,nt))
     CALL sep_read(srcwave_in, "srcwave_in")
     CALL init_srcwave(srcwave_in)
  ENDIF

  !-----------------------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> BORN FORWARD <<<<<<<<<<<<<<<<<<<<<<<"
  CALL L_lop2(.FALSE., .FALSE., modboth, datatmp, isou)
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> BORN ADJOINT <<<<<<<<<<<<<<<<<<<<<<<"
  CALL L_lop2(.TRUE., .FALSE., gnphi, datatmp, isou)
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> WEMVA FORWARD <<<<<<<<<<<<<<<<<<<<<<"
  CALL W_lop2(.TRUE., .FALSE., deltaB, modboth, isou)
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> sum(gnphi) = ", SUM(gnphi)
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> sum(deltaB) = ", SUM(deltaB)

  !-----------------------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> COMBINE COMPONENTS <<<<<<<<<<<<<<<<<"
  finalvel = gnphi + deltaB
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> sum(finalvel) = ", SUM(finalvel)

  !-----------------------------------------------------------------------------
  IF (verbose) WRITE(0,*) ">>>>>>>>>>>>>>>> Write output <<<<<<<<<<<<<<<<<<<<<<<"
  CALL sep_write(finalvel)

  !-----------------------------------------------------------------------------
  CALL DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  WRITE(0,*) "timer sum (sec)", timer_sum/1000
  WRITE(0,*) "timer sum (min)", timer_sum/1000/60
  WRITE(0,*) "DONE!"
  !-----------------------------------------------------------------------------

  IF (exist_file('test')) THEN
     CALL to_history("n1", n1, 'test')
     CALL to_history("o1", o1, 'test')
     CALL to_history("d1", d1, 'test')
     CALL to_history("n2", n2, 'test')
     CALL to_history("o2", o2, 'test')
     CALL to_history("d2", d2, 'test')
     CALL sep_write(finalvel, 'test')
  ENDIF

  IF (exist_file('gncomp')) THEN
     CALL to_history("n1", n1, 'gncomp')
     CALL to_history("o1", o1, 'gncomp')
     CALL to_history("d1", d1, 'gncomp')
     CALL to_history("n2", n2, 'gncomp')
     CALL to_history("o2", o2, 'gncomp')
     CALL to_history("d2", d2, 'gncomp')
     CALL sep_write(gnphi, 'gncomp')
  ENDIF

  IF (exist_file('wemvacomp')) THEN
     CALL to_history("n1", n1, 'wemvacomp')
     CALL to_history("o1", o1, 'wemvacomp')
     CALL to_history("d1", d1, 'wemvacomp')
     CALL to_history("n2", n2, 'wemvacomp')
     CALL to_history("o2", o2, 'wemvacomp')
     CALL to_history("d2", d2, 'wemvacomp')
     CALL sep_write(deltaB, 'wemvacomp')
  ENDIF

END PROGRAM FULL_HESSIAN
