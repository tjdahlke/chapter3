!==============================================================
!		GRADIENT MODULE	
!
!	The OMP loops in here don't seem to interfere with outer
!	loops over this operator.
!==============================================================


module gradient_mod
	implicit none
	real, private    	:: h
	integer, private    :: np, i, j, k

	contains
	!==============================================================


	subroutine gradient_init(np_in, h_in)
		integer			:: np_in
		real			:: h_in
		np = np_in
		h = h_in
	end subroutine



	subroutine gradient_op(add, adj, input, output)
		real, dimension(:)	:: input, output
		logical				:: add, adj
		if (adj) then
			if (.not. add) input=0.0
			! Edge gradient values
			input(2) = input(2)  + output(1)
			input(1) = input(1)  - output(1) 
			input(1) = input(1)/h
			input(np) = input(np) + output(np)
			input(np-1) = input(np-1) - output(np)
			input(np) = input(np)/h
			! Middle gradient values
			!$omp parallel do
		    do j=2,(np-1)
				input(j+1) = input(j+1) + output(j)
				input(j-1) = input(j-1) - output(j)
				input(j) = input(j)/(2*h)
			end do
			!$omp end parallel do					
		else
			if (.not. add) output=0.0
			! Edge gradient values
			output(1) = output(1) + input(2) 
			output(1) = output(1) - input(1) 
			output(1) = output(1)/h
			output(np) = output(np) + input(np)
			output(np) = output(np) - input(np-1)
			output(np) = output(np)/h
			! Middle gradient values
			!$omp parallel do
		    do j=2,(np-1)
				output(j) = output(j) + input(j+1)
				output(j) = output(j) - input(j-1)
				output(j) = output(j)/(2*h)
			end do
			!$omp end parallel do					
		endif
	end subroutine
	

end module