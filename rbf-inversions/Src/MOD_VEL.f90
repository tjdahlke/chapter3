program MOD_VEL
use sep
use mem_mod
use calc_mod
implicit none

	real                  				:: vsalt, beta, p2, dz, dx, oz, ox, alpha, mvphigrad
	integer								:: nz, nx
	logical								:: buildonly, drlseonly, verbose, usedrlse
	real, dimension(:,:), allocatable	:: phi, vel_back, tomo_grad, drlse_term, phi_grad,vel_full

	call sep_init()

	call from_param("verbose", verbose,.false.)
	call from_param("vsalt", vsalt, 4.5)
	CALL from_aux("vel_back", "n1", nz)
	CALL from_aux("vel_back", "n2", nx)
	CALL from_aux("vel_back", "d1", dz)
	CALL from_aux("vel_back", "d2", dx)
	CALL from_aux("vel_back", "o1", oz)
	CALL from_aux("vel_back", "o2", ox)

	call from_param("beta", beta, 0.0)
	call from_param("alpha", alpha, 0.0)
	call from_param("buildonly", buildonly,.false.)
	call from_param("drlseonly", drlseonly,.false.)
	call from_param("usedrlse", usedrlse,.true.)

!---------------- Allocate -------------------------------
	allocate(phi(nz, nx))
	allocate(phi_grad(nz, nx))
	allocate(vel_back(nz, nx))
	allocate(tomo_grad(nz, nx))
	allocate(drlse_term(nz, nx))
	allocate(vel_full(nz, nx))

!------------ Read in values ------------------------------
	call sep_read(phi, "phi")
	call sep_read(vel_back, "vel_back")

!------------ If only DRLSE ------------------------------
	if (drlseonly) then
		if (verbose) write(0,*) " -------------------------------------------"
		if (verbose) write(0,*) " DRLSE ONLY; NO GRADIENT"
		phi_grad	= 0.0
		tomo_grad	= 0.0
		call sep_read(drlse_term, "drlse_term")
	else
!------------ If only assembling ------------------------------
		if (buildonly) then
			if (verbose) write(0,*) " -------------------------------------------"
			if (verbose) write(0,*) " BUILD MODEL ONLY; NO GRADIENT"
			phi_grad	= 0.0
			drlse_term	= 0.0
			tomo_grad	= 0.0
		else

			if (exist_file('phi_grad'  )) then
				if (verbose) write(0,*) "Reading in PHI_GRAD"
				call sep_read(phi_grad, "phi_grad")
			else
				phi_grad=0.0
			end if

			if (verbose) write(0,*) "sum(phi_grad) = ", sum(phi_grad)
			if (exist_file('tomo_grad'  )) then
				if (verbose) write(0,*) "Reading in TOMO_GRAD"
				call sep_read(tomo_grad, "tomo_grad")
			else
				tomo_grad=0.0
			end if

			if (verbose) write(0,*) "sum(tomo_grad) = ", sum(tomo_grad)
			if (exist_file('drlse_term')) then
				if (verbose) write(0,*) "Reading in DRLSE_TERM"
				call sep_read(drlse_term, "drlse_term")
			else
				drlse_term=0.0
			end if
			if (verbose) write(0,*) "sum(drlse_term) = ", sum(drlse_term)

		end if
	end if

if (verbose) write(0,*) "beta = ", beta
if (verbose) write(0,*) "alpha = ", alpha

! Maxbeta is calculated based on a normalized gradient FYI
if (verbose) write(0,*) "-------------------------------------------------------"
if (verbose) write(0,*) "------------- Normalizing phigrad and tomograd --------"
mvphigrad=(maxval(phi_grad))
if (mvphigrad>0.0) then
	phi_grad = phi_grad/mvphigrad
	tomo_grad = tomo_grad/mvphigrad
else
	phi_grad = 0.0
	tomo_grad = 0.0
end if

	if (verbose) write(0,*) "maxval(phi) BEFORE = ", maxval(phi)

!------------ Calculate implicit surfaces ------------------------------
if (usedrlse) then
	if (verbose) write(0,*) "Using DRLSE term"
	phi = phi + phi_grad*beta + drlse_term
else
	if (verbose) write(0,*) "NOT Using DRLSE term"
	phi = phi + phi_grad*beta
endif

	if (verbose) write(0,*) "sum(phi_grad) = ", sum(phi_grad)
	if (verbose) write(0,*) "sum(phi_grad*beta) = ", sum(phi_grad*beta)
	if (verbose) write(0,*) "sum(vel_back) BEFORE = ", sum(vel_back)

	vel_back = vel_back + tomo_grad*alpha

	if (verbose) write(0,*) "sum(vel_back) AFTER  = ", sum(vel_back)

!-----------  Calculate full-velocity model  ----------------------------
	call calc_vel_init(nz, nx)
	call calc_velocity(phi, vel_back, vel_full, vsalt) ! Have phi2=phi in this case
	if (verbose) write(0,*) " sum(vel_full) = ", sum(vel_full)

!-----------  Output L1 norm of full-velocity model  ----------------------------
	if (verbose) write(0,*) "maxval(phi) AFTER = ", maxval(phi)


!-----------  Write outputs ------------------------------------------------
	if (exist_file('vel_full_test')) then
 		call to_history("n1",nz, 'vel_full_test')
 		call to_history("n2",nx, 'vel_full_test')
		call to_history("d1",dz, 'vel_full_test')
		call to_history("d2",dx, 'vel_full_test')
		call to_history("o1",oz, 'vel_full_test')
		call to_history("o2",ox, 'vel_full_test')
		call to_history("label1",'z [km]', 'vel_full_test')
		call to_history("label2",'x [km]', 'vel_full_test')
		call sep_write(SNGL(vel_full),	'vel_full_test'		)
	end if


	if (exist_file('phi_out')) then
		call to_history("n1",nz, 'phi_out')
		call to_history("n2",nx, 'phi_out')
		call to_history("d1",dz, 'phi_out')
		call to_history("d2",dx, 'phi_out')
		call to_history("o1",oz, 'phi_out')
		call to_history("o2",ox, 'phi_out')
		call to_history("label1",'z [km]', 'phi_out')
		call to_history("label2",'x [km]', 'phi_out')
		call sep_write(SNGL(phi),	 'phi_out')
	end if


	if (exist_file('vel_back_out')) then
 		call to_history("n1",nz, 'vel_back_out')
 		call to_history("n2",nx, 'vel_back_out')
		call to_history("d1",dz, 'vel_back_out')
		call to_history("d2",dx, 'vel_back_out')
		call to_history("o1",oz, 'vel_back_out')
		call to_history("o2",ox, 'vel_back_out')
		call to_history("label1",'z [km]', 'vel_back_out')
		call to_history("label2",'x [km]', 'vel_back_out')
		call sep_write(SNGL(vel_back), 'vel_back_out')
	end if


if (verbose) write(0,*) "MOD_VEL program complete"
end program
