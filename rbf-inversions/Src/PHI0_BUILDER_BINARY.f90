
!===============================================================================
!		Makes a binary valued implicit surface for either 2D or 3D models
!===============================================================================

program PHI_BUILDER_BINARY
use sep
implicit none

	integer										:: n1, n2, n3
	logical										:: verbose, slowsalt
	real										:: thresh
	real, dimension(:,:), allocatable			:: input2d, output2d
	real, dimension(:,:,:), allocatable			:: input3d, output3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("thresh",thresh,0.0)
	call from_param("slowsalt",slowsalt,.false.)

	call from_history("n1",n1)
	call from_history("n2",n2)
	call from_history("n3",n3,1)

	if(verbose) write(0,*) "thresh = ", thresh
	if(verbose) write(0,*) "slowsalt = ", slowsalt
	if(verbose) write(0,*) "maxval(input2d) = ", maxval(input2d)


	if (n3==1) then
		if(verbose) write(0,*) "========= Detected 2D input ==============="
		allocate(input2d(n1, n2))
		allocate(output2d(n1, n2))
		call sep_read(input2d)
		output2d=-1.0
		WHERE (input2d >thresh) output2d = 1.0
		if(verbose) write(0,*) "maxval(output2d) = ", maxval(output2d)
		if (slowsalt) then
			output2d=-1.0*output2d
		endif
		call sep_write(output2d)
	else
		if(verbose) write(0,*) "========= Detected 3D input ==============="
		allocate(input3d(n1, n2, n3))
		allocate(output3d(n1, n2, n3))
		call sep_read(input3d)
		output3d=-1.0
		WHERE (input3d >thresh) output3d = 1.0
		if(verbose) write(0,*) "maxval(output3d) = ", maxval(output3d)
		if (slowsalt) then
			output3d=-1.0*output3d
		endif
		call sep_write(output3d)
	end if

end program
