
!===================================================================================
!		Assemble the velocity model from the implicit surface and background vel
!===================================================================================

program MAKE_VEL_MODEL
use sep
implicit none

	integer										:: nz, ny, nx
	logical										:: verbose
	real										:: vsalt, lowcut, hicut
	real, dimension(:,:), allocatable			:: Heavi2d, velback2d, saltvel2d, final2d, watermask2d
	real, dimension(:,:,:), allocatable			:: Heavi3d, velback3d, saltvel3d, final3d, watermask3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("vsalt",vsalt,4500.0)
	call from_param("hicut",hicut,3500.0)
	call from_param("lowcut",lowcut,1500.0)

	call from_aux("heaviside", "n1", nz)
	call from_aux("heaviside", "n2", ny)
	call from_aux("heaviside", "n3", nx,1)

	! Read in the watermask if it exists
	IF (exist_file('watermask') .and. (nx==1)) THEN
		ALLOCATE(watermask2d(nz,ny))
		CALL sep_read(watermask2d, "watermask")
	ELSE IF (exist_file('watermask') .and. (nx/=1)) THEN
		ALLOCATE(watermask3d(nz,ny,nx))
		CALL sep_read(watermask3d, "watermask")		
	END IF


	if (nx==1) then
		if(verbose) write(0,*) "========= Detected 2D inputs ==============="
		allocate(Heavi2d(nz, ny))
		allocate(saltvel2d(nz, ny))
		allocate(velback2d(nz, ny))
		allocate(final2d(nz, ny))
		call sep_read(velback2d)
		call sep_read(Heavi2d,"heaviside")
		saltvel2d=vsalt

		IF (exist_file('watermask')) THEN
			Heavi2d = Heavi2d*watermask2d
		END IF

		! Clip the background velocity so that it isnt too low or too high
		WHERE (velback2d > hicut) velback2d=hicut
		WHERE (velback2d < lowcut) velback2d=lowcut

		final2d=(saltvel2d-velback2d)*Heavi2d + velback2d
		call sep_write(final2d)
	else
		if(verbose) write(0,*) "========= Detected 3D inputs ==============="
		allocate(Heavi3d(nz, ny, nx))
		allocate(saltvel3d(nz, ny, nx))
		allocate(velback3d(nz, ny, nx))
		allocate(final3d(nz, ny, nx))
		call sep_read(velback3d)
		call sep_read(Heavi3d,"heaviside")
		saltvel3d=vsalt

		IF (exist_file('watermask')) THEN
			Heavi3d = Heavi3d*watermask3d
		END IF

		! Clip the background velocity so that it isnt too low or too high
		WHERE (velback3d > hicut) velback3d=hicut
		WHERE (velback3d < lowcut) velback3d=lowcut

		final3d=(saltvel3d-velback3d)*Heavi3d + velback3d
		call sep_write(final3d)
	end if

end program









