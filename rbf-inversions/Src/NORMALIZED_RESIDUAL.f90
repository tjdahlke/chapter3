program NORMALIZED_RESIDUAL
  !
  ! Inputs:
  !   1) history file of obs_data
  !   2) history file of syn_data

  ! Outputs:
  !   1) history file of normalized residual data
  !
  ! Taylor Dahlke, October 2018
  !
  !VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

  !=================================
  ! Standard declarations
  !=================================
  use sep
  implicit none

  !=================================
  ! Declare variables
  !=================================
  ! Program variables
  integer                           :: ii,ntraces,nt
  real                              :: obsnorm,synnorm,ot
  real, dimension(:),allocatable    :: obstraceO,syntraceO,obstraceN,syntraceN,Rd
  call sep_init()

  ! History file parameters
  call from_history('n1',nt)
  call from_history('n2',ntraces)

  call from_aux("obsdata","o1",ot)
  call to_history('o1',ot)

  ! Make arrays
  allocate(obstraceO(nt))
  allocate(syntraceO(nt))
  allocate(obstraceN(nt))
  allocate(syntraceN(nt))
  allocate(Rd(nt))


  ! Make virtual source traces
  do ii=0,ntraces

      ! Read the traces
      write(0,*) "percent done: ", 100*ii/ntraces
      call sep_read(syntraceO)
      call sep_read(obstraceO,"obsdata")

      ! Find trace norms
      synnorm=SQRT(DOT_PRODUCT(syntraceO,syntraceO))
      obsnorm=SQRT(DOT_PRODUCT(obstraceO,obstraceO))

      if ( ((synnorm**3)/=0.0) .and. (obsnorm/=0.0)) then
          ! Only normalize when norms are non-zero
          syntraceN = syntraceO/synnorm
          obstraceN = obstraceO/obsnorm
      else
          Rd=0.0
          call sep_write(Rd)
          ! Dont bother doing the rest in this case
          cycle
      end if

      ! Residual of normalized traces
      Rd = obstraceN - syntraceN 

      ! Write the output trace
      call sep_write(Rd)

  end do

  write(0,*) " DONE!"

end program




