
//==============================================================================================
//
//      2D BORN APPROXIMATION CODE
//      Taylor Dahlke, taylor@sep.stanford.edu, 10/23/2017
//
//      This program runs the Born approximation to the acoustic wave equation. It uses 
//      TBB vectorization for the inner loop to speed things up. I follow the formulation
//      described in the paper:
//
//      "Accurate implementation of two-way wave-equation operators", Ali Almomim
//
//      Absorbing boundary conditions are simple dampening in a padding region around the model.
//
//      FORWARD OPERATOR:  
//         reflectivity space (Nz x NY) ---->>  residual space (NT x NREC)
//
//      ADJOINT OPERATOR:  
//         residual space (NT x NREC) ---->>  reflectivity space (Nz x NY)
//
//==============================================================================================

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacianOP2d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;
typedef boost::multi_array<float, 2> float2D;


class block {
public:
block(int b1,int e1,int b2,int e2) : _b1(b1),_e1(e1),_b2(b2),_e2(e2){
};
int _b1,_e1,_b2,_e2;
};



int main(int argc, char **argv){

        timer x=timer("FULL RUN");
        timer x1=timer("INITIALIZATION");
        timer x2=timer("MAIN WAVEFIELD LOOP");
        x.start();
        x1.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> velin=io->getRegFile("vel",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<hypercube> hyperVel=velin->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();

        //==========================================================================
        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long ny=velN[1];
        float dz = hyperVel->getAxis(1).d;
        float dy = hyperVel->getAxis(2).d;
        float oz = hyperVel->getAxis(1).o;
        float oy = hyperVel->getAxis(2).o;

        fprintf(stderr, "nz=%d\n",nz );
        fprintf(stderr, "ny=%d\n",ny );

        //==========================================================================
        // Get acquisition parameters
        std::vector<int> recN=hyperRec->getNs();
        int Nkeys=recN[0];
        int Nrec=recN[1];

        //==========================================================================
        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("d1",0.1);
        float dt=par->getFloat("dt",0.002);
        int adj=par->getInt("adj",0);
        int cfl=par->getInt("cfl",0);
        int souId=par->getInt("souId",1);
        int maxYlag=par->getInt("maxYlag",0);
        int dlag=par->getInt("dlag",3);
        int pad=par->getInt("pad",65);

        int ext=0;
        if(maxYlag>0){
                ext=1;
        }
        int nlags=(2*maxYlag+1);

        SEP::axis axT(nt,0.0,dt);
        SEP::axis axR(Nrec,0.0,1.0);
        SEP::axis axZ = hyperVel->getAxis(1);
        SEP::axis axY = hyperVel->getAxis(2);
        SEP::axis axK(Nkeys,0.0,1.0);
        SEP::axis axL(nlags,-maxYlag*dlag,dlag);

        x1.stop();
        x1.print();
        x1.start();

        fprintf(stderr, "Nkeys =%d\n", Nkeys);
        fprintf(stderr, "Nrec =%d\n", Nrec);
        fprintf(stderr, "nt =%d\n", nt);
        fprintf(stderr, "ext =%d\n", ext);
        fprintf(stderr, "maxYlag =%d\n", maxYlag);


        //==========================================================================
        // Auxilary inputs and outputs

        // Read in the source wavelet
        fprintf(stderr, "WAVELET INPUT\n" );
        std::shared_ptr<genericRegFile> wav;
        std::shared_ptr<hypercube> hyperWav(new hypercube(axT));
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav));
        wav=io->getRegFile("wavelet",usageIn);
        wav->readFloatStream(wavelet->getVals(),hyperWav->getN123()); 

        // Setup the velocity model input
        SEP::axis ax1D(ny*nz,0.0,1.0);
        SEP::axis ax1Dlag(ny*nz*nlags,0.0,1.0);
        std::shared_ptr<hypercube> hyperVel1d(new hypercube(ax1D));
        std::shared_ptr<hypercube> hyperVel1dLag(new hypercube(ax1Dlag));
        std::shared_ptr<float1DReg> velModel(new float1DReg(hyperVel1d));
        velin->readFloatStream(velModel->getVals(),hyperVel->getN123());
        std::shared_ptr<hypercube> hyperVelLag(new hypercube(axZ,axY,axL));

        // Read in the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float2D> abcW2(new float2D(boost::extents[ny][nz]));
        int useabcs=par->getInt("useabcs",1);
        if(useabcs){
                fprintf(stderr, "Using ABC dampening \n");
                std::shared_ptr<genericRegFile> abc=io->getRegFile("abc",usageIn);
                std::shared_ptr<float1DReg> abcW(new float1DReg(hyperVel1d));
                abc->readFloatStream(abcW->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < ny*nz; ii++) {
                        abcW2->data()[ii] = (*abcW->_mat)[ii];
                }
        }
        else{
                fprintf(stderr, "Using no ABC dampening \n");
                for(int ii=0; ii < ny*nz; ii++) {
                        abcW2->data()[ii] = 0.0;
                }
        }
        fprintf(stderr,">>>>> Done reading ABC's \n");
        
// ==============================================================================
        // WAVEFIELD OUTPUT
        int wavefield=par->getInt("wavefield",0);
        fprintf(stderr, "wavefield=%d\n", wavefield);
        std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axY,axT));
        std::shared_ptr<hypercube> hyperOutFrame(new hypercube(axZ,axY));
        std::shared_ptr<float2DReg> waveFrameS(new float2DReg(hyperOutFrame));
        std::shared_ptr<float2DReg> waveFrameR(new float2DReg(hyperOutFrame));
        std::shared_ptr<float2DReg> waveFrameI(new float2DReg(hyperOutFrame));
        std::shared_ptr<genericRegFile> outWVR;
        std::shared_ptr<genericRegFile> outWVS;
        std::shared_ptr<genericRegFile> outWVI;
        if (wavefield){
                outWVS=io->getRegFile("waveS",usageOut);
                outWVS->setHyper(hyperOut3d);
                outWVS->writeDescription();
                fprintf(stderr,">>>>> Done setting up the receiver wavefield output \n");
                outWVR=io->getRegFile("waveR",usageOut);
                outWVR->setHyper(hyperOut3d);
                outWVR->writeDescription();
                fprintf(stderr,">>>>> Done setting up the source wavefield output \n");
                outWVI=io->getRegFile("waveI",usageOut);
                outWVI->setHyper(hyperOut3d);
                outWVI->writeDescription();
                fprintf(stderr,">>>>> Done setting up the source wavefield output \n");
        }
// ==============================================================================


        // TEST VEL OUTPUT
        std::shared_ptr<hypercube> hyperOut2dv(new hypercube(axZ,axY));
        std::shared_ptr<float2DReg> testvel(new float2DReg(hyperOut2dv));
        std::shared_ptr<genericRegFile> outTV;
        outTV=io->getRegFile("testvel",usageOut);
        outTV->setHyper(hyperOut2dv);
        outTV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the testvel output \n");


        x1.stop();
        x1.print();
        x1.start();

        //==========================================================================
        // Model input/output
        std::shared_ptr<genericRegFile> refl;
        std::shared_ptr<float3D> model2ext(new float3D(boost::extents[nlags][ny][nz]));
        std::shared_ptr<float1DReg> model1ext(new float1DReg(hyperVel1dLag));
        std::shared_ptr<float2D> model2(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float1DReg> model1(new float1DReg(hyperVel1d));

        if(adj){// DATA OUTPUT
                fprintf(stderr, "REFLECTIVITY OUTPUT\n" );
                refl=io->getRegFile("out",usageOut);
                if (ext){
                        refl->setHyper(hyperVelLag);
                }
                else{
                        refl->setHyper(hyperVel);
                }
                refl->writeDescription();
        }
        else{   // REFLECTIVITY INPUT
                fprintf(stderr, "REFLECTIVITY INPUT\n" );
                refl=io->getRegFile("in",usageIn);
                refl->readFloatStream(model1->getVals(),hyperVel1d->getN123());  
                for(int ii=0; ii < ny*nz; ii++) {
                        model2->data()[ii] = (*model1->_mat)[ii];
                } 
        }
        x1.stop();
        x1.print();
        x1.start();


        //==========================================================================
        // Shot gather data
        std::shared_ptr<hypercube> hyperShotG(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> data(new float2DReg(hyperShotG));
        std::shared_ptr<genericRegFile> shotG;
        // Shot gather 3D header
        std::shared_ptr<hypercube> hyperShotGH(new hypercube(axK,axR));
        std::shared_ptr<float2DReg> dataH(new float2DReg(hyperShotGH));
        std::shared_ptr<genericRegFile> shotGH;
        if(adj){// SHOT GATHER INPUT
                fprintf(stderr, "SHOT GATHER INPUT\n" );
                shotG=io->getRegFile("in",usageIn);
                shotG->readFloatStream(data->getVals(),hyperShotG->getN123());    
        }
        else{   // SHOT GATHER OUTPUT
                fprintf(stderr, "SHOT GATHER OUTPUT\n" );
                shotG=io->getRegFile("out",usageOut);
                shotG->setHyper(hyperShotG);
                shotG->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather output \n");
                fprintf(stderr, "SHOT GATHER 3D HEADER OUTPUT\n" );
                shotGH=io->getRegFile("header",usageOut);
                shotGH->setHyper(hyperShotGH);
                shotGH->writeDescription();
                fprintf(stderr,">>>>> Done setting up the shotgather 3D spatial header output \n");
        }
        //==========================================================================
        x1.stop();
        x1.print();
        x1.start();


        //==========================================================================
        // Read in the background velocity model (v0) and scale
        std::shared_ptr<float2D> v2dt(new float2D(boost::extents[ny][nz]));
        if(cfl){
                float maxvel=(*velModel->_mat)[0];
                for(int ii=0; ii < ny*nz; ii++) {
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                        if ( (*velModel->_mat)[ii] > maxvel){
                                maxvel = (*velModel->_mat)[ii];
                        }
                }
                // Check the CFL condition
                float cfl=maxvel*dt/dsamp;
                float compval=sqrt(2.0)/2.0;
                if (cfl>=compval){
                        fprintf(stderr,">>>>> ERROR: %f > %f \n",cfl,compval);
                        fprintf(stderr,">>>>> maxvel=%f  dt=%f  dsamp=%f \n",maxvel,dt,dsamp);
                        fprintf(stderr,"Doesn't pass the CFL condition. Either decrease dt or increase dsamp\n");
                        exit (EXIT_FAILURE);
                }
                fprintf(stderr,">>>>> PASSED the CFL condition \n");
        }
        else{
                float maxvel=(*velModel->_mat)[0];
                for(int ii=0; ii < ny*nz; ii++) {
                        v2dt->data()[ii] = ((*velModel->_mat)[ii])*((*velModel->_mat)[ii])*dt*dt;
                }
                fprintf(stderr,">>>>> Did not check the CFL condition \n");
        }
        fprintf(stderr,">>>>> Done reading / scaling the velocity model \n");



        x1.stop();
        x1.print();
        x1.start();

        fprintf(stderr, "Initializing temp arrays \n");

        // =====================================================================
        // Initialize laplacian FD coefficients and pressure fields

        float coef[6];
        float sc=1.0/(dsamp*dsamp);
        coef[1]=sc*42000/(25200.0);
        coef[2]=-1.0*sc*6000/(25200.0);
        coef[3]=sc*1000/(25200.0);
        coef[4]=-1.0*sc*125/(25200.0);
        coef[5]=sc*8/(25200.0);
        coef[0]=-4*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);
        int numCoeff = (sizeof(coef)/4)-1;
        if (maxYlag*dlag>5){
                numCoeff = maxYlag;
        }
        fprintf(stderr, "numCoeff=%d\n",numCoeff);

        std::shared_ptr<float2D> oldS(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curS(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> newS(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> newFS(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> tmpS(new float2D(boost::extents[ny][nz]));

        std::shared_ptr<float2D> oldR(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curR(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> newR(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> newFR(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> tmpR(new float2D(boost::extents[ny][nz]));

        std::shared_ptr<float2D> oldTMP(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curTMP(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> newTMP(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> temp(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curTemp1(new float2D(boost::extents[ny][nz]));

        std::shared_ptr<float2D> curRZ(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curRY(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> curD(new float2D(boost::extents[ny][nz]));
        std::shared_ptr<float2D> finalP(new float2D(boost::extents[ny][nz]));


        fprintf(stderr,">>>>> Zeroing out the arrays \n");
        std::fill( newS->data(), newS->data() + newS->num_elements(), 0 ); 
        std::fill( newFS->data(), newFS->data() + newFS->num_elements(), 0 ); 
        std::fill( newR->data(), newR->data() + newR->num_elements(), 0 ); 
        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
        std::fill( oldR->data(), oldR->data() + oldR->num_elements(), 0 ); 
        std::fill( oldS->data(), oldS->data() + oldS->num_elements(), 0 ); 
        std::fill( curR->data(), curR->data() + curR->num_elements(), 0 ); 
        std::fill( curS->data(), curS->data() + curS->num_elements(), 0 ); 
        std::fill( oldTMP->data(), oldTMP->data() + oldTMP->num_elements(), 0 ); 
        std::fill( curTMP->data(), curTMP->data() + curTMP->num_elements(), 0 ); 
        std::fill( newTMP->data(), newTMP->data() + newTMP->num_elements(), 0 ); 
        std::fill( temp->data(), temp->data() + temp->num_elements(), 0 ); 
        std::fill( curTemp1->data(), curTemp1->data() + curTemp1->num_elements(), 0 ); 


        // //=====================================================================
        x1.stop();
        x1.print();
        x1.start();     
        if(adj){// READ WAVEFIELD PREVIOUS FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD PREVIOUS FRAME INPUT \n");
                std::shared_ptr<float1DReg> old2(new float1DReg(hyperVel1d));
                std::shared_ptr<genericRegFile> inPRE;
                inPRE=io->getRegFile("curW",usageIn); // Switched for time reversal
                inPRE->readFloatStream(old2->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < ny*nz; ii++) {
                        oldS->data()[ii] = (*old2->_mat)[ii];
                }
        }
        x1.stop();
        x1.print();
        x1.start();
        if(adj){ // READ WAVEFIELD CURRENT FRAME INPUT
                fprintf(stderr, "READ WAVEFIELD CURRENT FRAME INPUT \n");
                std::shared_ptr<float1DReg> cur2(new float1DReg(hyperVel1d));
                std::shared_ptr<genericRegFile> inCUR;
                inCUR=io->getRegFile("oldW",usageIn); // Switched for time reversal
                inCUR->readFloatStream(cur2->getVals(),hyperVel1d->getN123());
                for(int ii=0; ii < ny*nz; ii++) {
                        curS->data()[ii] = (*cur2->_mat)[ii];
                }
        }


        x1.stop();
        x1.print();
        x1.start();

        //=====================================================================
        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2;
        bs1=bs;
        bs2=bs;

        vector<int> b1(1,numCoeff),e1(1,numCoeff+bs1); // "5" comes from the 5 coefficient arm of the Laplacian stencil
        int nleft=nz-2*numCoeff-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,numCoeff),e2(1,numCoeff+bs2);
        nleft=ny-2*numCoeff-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        // Make the full propagation regions blocks
        vector<block> blocks;
        for(int i2=0; i2<b2.size(); ++i2) {
                for(int i1=0; i1<b1.size(); ++i1) {
                        blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));
                }
        }

        // Make ABC block regions
        vector<block> Z1blocks;
        vector<block> Z2blocks;
        vector<block> Y1blocks;
        vector<block> Y2blocks;
        for(int i2=0; i2<b2.size(); ++i2){
                for(int i1=0; i1<b1.size(); ++i1){
                        // Z dimension blocks
                        if (b1[i1]<pad){
                                Z1blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));                 
                        }
                        if (e1[i1]>(nz-pad)){
                                Z2blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));
                        }
                        // Y dimension blocks (no overlap with Z blocks)
                        if ( (b2[i1]<pad)      and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                Y1blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));                 
                        }
                        if ( (e2[i1]>(ny-pad)) and (b1[i1]>=pad) and (e1[i1]<=(nz-pad)) ){
                                Y2blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));
                        }
                }
        }

        x1.stop();
        x1.print();
        x1.start();

        //=====================================================================        
        // Specify source position
        int isou=0;
        int souz=par->getFloat("souZ");
        int souy=par->getFloat("souY");
        int soux=par->getFloat("souX");
        fprintf(stderr,"souz (index) = %d\n",souz);
        fprintf(stderr,"souy (index) = %d\n",souy);
        fprintf(stderr,"soux (index) = %d\n",soux);


        int souyz=souz+souy*nz;
        float v2dti=-1.0*((*velModel->_mat)[souyz])*((*velModel->_mat)[souyz])*dt*dt;
        float dt2=dt*dt;
        int ylag;
        int ABSylag;

        //========  MAIN WAVEFIELD LOOP ========================================

        x1.stop();
        x1.print();
        x2.start();

        if (adj){ // ADJOINT   
                fprintf(stderr, "ADJOINT OPERATOR \n");

                // Kick start the source wavefield using (*wavelet->_mat)[nt-1] as the source
                for(int it=0; it < 2; it++) {
                        // Inject receiver recordings (reverse time)
                        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                (*newFR)[recy][recz]=(*newFR)[recy][recz]+(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][souz] = v2dti*(*wavelet->_mat)[nt-1];

                        // Inject and propagate source wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                laplacianFOR2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,v2dt->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                        }
                                }
                        });
                        // Extract the source wavefield for debugging
                        if (wavefield){
                                (*waveFrameS->_mat) = (*newS);
                                outWVS->writeFloatStream(waveFrameS->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }

                        // Adjoint stepping kernel (reverse time rec wavefield)
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        scatFOR2d(nz,v2dt->data()+i3*nz,curTemp1->data()+i3*nz,curR->data()+i3*nz);
                                }
                        });
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                waveADJ2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,oldR->data()+i,curTemp1->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                        }
                                }
                        });


                        // //==================================================
                        // // Make first-derivatives Z1
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z1blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Z1blocks[ib]._b2; i2 < Z1blocks[ib]._e2; i2++) {
                        //                         int i=Z1blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oZ(Z1blocks[ib]._e1-Z1blocks[ib]._b1,nz,curR->data()+i,curRZ->data()+i,dz,-1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Z2
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z2blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Z2blocks[ib]._b2; i2 < Z2blocks[ib]._e2; i2++) {
                        //                         int i=Z2blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oZ(Z2blocks[ib]._e1-Z2blocks[ib]._b1,nz,curR->data()+i,curRZ->data()+i,dz,1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Y1
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y1blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Y1blocks[ib]._b2; i2 < Y1blocks[ib]._e2; i2++) {
                        //                         int i=Y1blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oY(Y1blocks[ib]._e1-Y1blocks[ib]._b1,nz,curR->data()+i,curRY->data()+i,dz,-1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Y2
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y2blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Y2blocks[ib]._b2; i2 < Y2blocks[ib]._e2; i2++) {
                        //                         int i=Y2blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oY(Y2blocks[ib]._e1-Y2blocks[ib]._b1,nz,curR->data()+i,curRY->data()+i,dz,1.0);
                        //                 }
                        //         }
                        // });

                        // // ADD IN THE ALI-STYLE ABCS
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                        //         for(int i3=r.begin(); i3!=r.end(); ++i3){
                        //                 sumABCs(nz,curRZ->data()+i3*nz,curRY->data()+i3*nz,curD->data()+i3*nz);
                        //                 addABC(nz,dt,abcW2->data()+i3*nz,v2dt->data()+i3*nz,curR->data()+i3*nz,curD->data()+i3*nz,newR->data()+i3*nz,finalP->data()+i3*nz);
                        //                 simple_mult(nz,finalP->data()+i3*nz,newR->data()+i3*nz,1.0);
                        //         }
                        // });
                        // //==================================================


                        // Extract the receiver wavefield for debugging
                        if (wavefield){
                                (*waveFrameR->_mat) = (*newR);
                                outWVR->writeFloatStream(waveFrameR->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }

                        if (ext){
                                // Extended imaging
                                for(int iy=0; iy!=(nlags+1); ++iy){
                                        ylag=(iy-maxYlag)*dlag;
                                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                                int i=blocks[ib]._b1+i2*nz;
                                                                comboBornADJ2d(blocks[ib]._e1-blocks[ib]._b1,v2dt->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,temp->data()+i,dt2);
                                                                scatADJ2dext(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,ylag,iy,newS->data()+i,temp->data()+i,model2ext->data()+i);
                                                        }
                                                }
                                        });
                                }
                        }
                        else{
                                // Combine scaling, second time derivative, and imaging condition into one function for efficiency sake                                        
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                                        for(int i3=r.begin(); i3!=r.end(); ++i3){
                                                comboBornADJ2d(nz,v2dt->data()+i3*nz,oldR->data()+i3*nz,curR->data()+i3*nz,newR->data()+i3*nz,temp->data()+i3*nz,dt2);
                                                scatADJ2d(nz,newS->data()+i3*nz,temp->data()+i3*nz,model2->data()+i3*nz);
                                        }
                                });     
                        }
                        // Extract the imaging time series for debugging
                        if (wavefield){
                                (*waveFrameI->_mat) = (*model2);
                                outWVI->writeFloatStream(waveFrameI->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }


                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }

                fprintf(stderr, "ADJOINT OPERATOR main loop\n");

                // MAIN LOOP
                for(int it=2; it < nt; it++) {
                        // Inject receiver recordings (reverse time)
                        std::fill( newFR->data(), newFR->data() + newFR->num_elements(), 0 ); 
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                (*newFR)[recy][recz]=(*newFR)[recy][recz]+(*data->_mat)[irec][nt-1-it];
                        }
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][souz] = v2dti*(*wavelet->_mat)[nt+1-it];

                        // Inject and propagate source wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                laplacianFOR2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,v2dt->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                        }
                                }
                        });
                        // Extract the source wavefield for debugging
                        if (wavefield){
                                (*waveFrameS->_mat) = (*newS);
                                outWVS->writeFloatStream(waveFrameS->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }


                        // Adjoint stepping kernel (reverse time rec wavefield)
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        scatFOR2d(nz,v2dt->data()+i3*nz,curTemp1->data()+i3*nz,curR->data()+i3*nz);
                                }
                        });
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                waveADJ2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,oldR->data()+i,curTemp1->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                        }
                                }
                        });

                        // //==================================================
                        // // Make first-derivatives Z1
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z1blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Z1blocks[ib]._b2; i2 < Z1blocks[ib]._e2; i2++) {
                        //                         int i=Z1blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oZ(Z1blocks[ib]._e1-Z1blocks[ib]._b1,nz,curR->data()+i,curRZ->data()+i,dz,-1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Z2
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Z2blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Z2blocks[ib]._b2; i2 < Z2blocks[ib]._e2; i2++) {
                        //                         int i=Z2blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oZ(Z2blocks[ib]._e1-Z2blocks[ib]._b1,nz,curR->data()+i,curRZ->data()+i,dz,1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Y1
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y1blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Y1blocks[ib]._b2; i2 < Y1blocks[ib]._e2; i2++) {
                        //                         int i=Y1blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oY(Y1blocks[ib]._e1-Y1blocks[ib]._b1,nz,curR->data()+i,curRY->data()+i,dz,-1.0);
                        //                 }
                        //         }
                        // });

                        // // Make first-derivatives Y2
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)Y2blocks.size()),[&](const tbb::blocked_range<int>&r){
                        //         for(int ib=r.begin(); ib!=r.end(); ++ib) {
                        //                 for(int i2=Y2blocks[ib]._b2; i2 < Y2blocks[ib]._e2; i2++) {
                        //                         int i=Y2blocks[ib]._b1+i2*nz;
                        //                         first_deriv2oY(Y2blocks[ib]._e1-Y2blocks[ib]._b1,nz,curR->data()+i,curRY->data()+i,dz,1.0);
                        //                 }
                        //         }
                        // });

                        // // ADD IN THE ALI-STYLE ABCS
                        // tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                        //         for(int i3=r.begin(); i3!=r.end(); ++i3){
                        //                 sumABCs(nz,curRZ->data()+i3*nz,curRY->data()+i3*nz,curD->data()+i3*nz);
                        //                 addABC(nz,dt,abcW2->data()+i3*nz,v2dt->data()+i3*nz,curR->data()+i3*nz,curD->data()+i3*nz,newR->data()+i3*nz,finalP->data()+i3*nz);
                        //                 simple_mult(nz,finalP->data()+i3*nz,newR->data()+i3*nz,1.0);
                        //         }
                        // });
                        // //==================================================

                        // Extract the receiver wavefield for debugging
                        if (wavefield){
                                (*waveFrameR->_mat) = (*newR);
                                outWVR->writeFloatStream(waveFrameR->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }


                        if (ext){
                                // Extended imaging
                                for(int iy=0; iy!=(nlags+1); ++iy){
                                        ylag=(iy-maxYlag)*dlag;
                                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                                int i=blocks[ib]._b1+i2*nz;
                                                                comboBornADJ2d(blocks[ib]._e1-blocks[ib]._b1,v2dt->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,temp->data()+i,dt2);
                                                                scatADJ2dext(blocks[ib]._e1-blocks[ib]._b1,nz,nz*ny,ylag,iy,newS->data()+i,temp->data()+i,model2ext->data()+i);
                                                        }
                                                }
                                        });
                                }
                        }
                        else{
                                // Combine scaling, second time derivative, and imaging condition into one function for efficiency sake                                        
                                tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                                        for(int i3=r.begin(); i3!=r.end(); ++i3){
                                                comboBornADJ2d(nz,v2dt->data()+i3*nz,oldR->data()+i3*nz,curR->data()+i3*nz,newR->data()+i3*nz,temp->data()+i3*nz,dt2);
                                                scatADJ2d(nz,newS->data()+i3*nz,temp->data()+i3*nz,model2->data()+i3*nz);
                                        }
                                });     
                        }
                        // Extract the imaging time series for debugging
                        if (wavefield){
                                (*waveFrameI->_mat) = (*model2);
                                outWVI->writeFloatStream(waveFrameI->getVals(),hyperOutFrame->getN123());  // Full wavefield at souy
                        }


                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }
        }
        else{ // FORWARD
                fprintf(stderr, "FORWARD OPERATOR \n");
                for(int it=0; it < nt; it++) {
                        fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                        // Scale and inject source
                        (*newFS)[souy][souz] = v2dti*(*wavelet->_mat)[it];

                        // Propagate the source wavefield
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                laplacianFOR2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,v2dt->data()+i,oldS->data()+i,curS->data()+i,newS->data()+i,newFS->data()+i);
                                        }
                                }
                        });
                        // Combine scattering, second time derivative, and scaling into one function for efficiency sake                                        
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)ny),[&](const tbb::blocked_range<int>&r){
                                for(int i3=r.begin(); i3!=r.end(); ++i3){
                                        comboBornFOR2d(nz,v2dt->data()+i3*nz,model2->data()+i3*nz,oldS->data()+i3*nz,curS->data()+i3*nz,newS->data()+i3*nz,newFR->data()+i3*nz,dt2);
                                }
                        });
                        // Propagate the scattered data (rec wavefield)
                        tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                                for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz;
                                                laplacianFOR2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,v2dt->data()+i,oldR->data()+i,curR->data()+i,newR->data()+i,newFR->data()+i);
                                        }
                                }
                        });

                        // Extract reciever recordings
                        for(int irec=0; irec < Nrec; irec++) {
                                int recz=(*reccoor->_mat)[irec][1];
                                int recy=(*reccoor->_mat)[irec][2];
                                (*data->_mat)[irec][it]=(*newR)[recy][recz];
                        }


                        // Update the wavefields
                        tmpS=oldS; oldS=curS; curS=newS; newS=tmpS;
                        tmpR=oldR; oldR=curR; curR=newR; newR=tmpR;
                }

                for(int irec=0; irec < Nrec; irec++){
                        // Write header info for each extracted trace
                        int nodeId=(*reccoor->_mat)[irec][0];
                        int recz=(*reccoor->_mat)[irec][1];
                        int recy=(*reccoor->_mat)[irec][2];
                        int recx=(*reccoor->_mat)[irec][3];
                        float offset=(*reccoor->_mat)[irec][7];
                        float gz=float(recz);
                        float gy=float(recy);
                        float gx=float(recx);
                        float sz=float(souz);
                        float sy=float(souy);
                        float sx=float(soux);
                        (*dataH->_mat)[irec][0]=nodeId;
                        (*dataH->_mat)[irec][1]=gz;
                        (*dataH->_mat)[irec][2]=gy;
                        (*dataH->_mat)[irec][3]=gx;
                        (*dataH->_mat)[irec][4]=sz;
                        (*dataH->_mat)[irec][5]=sy;
                        (*dataH->_mat)[irec][6]=sx;
                        (*dataH->_mat)[irec][7]=offset;
                }

        }
        x2.stop();
        x2.print();




        fprintf(stderr, "About to write out the debugging wavefield \n");
        // =============================================================================

        if (ext){
                if(adj){
                        fprintf(stderr, "Writing out the reflectivity \n");
                        for(int ii=0; ii < ny*nz*nlags; ii++) {
                                (*model1ext->_mat)[ii]=model2ext->data()[ii];
                        } 
                        refl->writeFloatStream(model1ext->getVals(),hyperVelLag->getN123());  // Wavelet
                }
        }
        else{
                if(adj){
                        fprintf(stderr, "Writing out the zero-lag reflectivity \n");
                        for(int ii=0; ii < ny*nz; ii++) {
                                (*model1->_mat)[ii]=model2->data()[ii];
                        } 
                        refl->writeFloatStream(model1->getVals(),hyperVel->getN123());  // Wavelet
                }
                else{
                        fprintf(stderr, "Writing out the shot gather \n");
                        shotG->writeFloatStream(data->getVals(),hyperShotG->getN123());         // Shot gather
                        shotGH->writeFloatStream(dataH->getVals(),hyperShotGH->getN123());      // Shot gather header info
                }
        }

        // Write the test vel output
        outTV->writeFloatStream(testvel->getVals(),hyperOut2dv->getN123());  // Full wavefield at souy


        x.stop();
        x.print();

}
