[general_parameters]
n1=400
airpad=105 
// If doing a hard boundary, need at least 5 points in airpad (assuming the laplacian is 10th order)
n2=1000
n3=1

d1=10.0
d2=10.0
d3=10.0

o1=-1050.0 // This is based on 0.0 - airpad*dz
o2=45010.0 // Doesnt include RBC or ABC padding
// For the 2D line
// o3=214250.0 
o3=214800
// 214831  // Doesnt include RBC or ABC padding

// For the full 3d extent
// 209010.0

// 		y, crossline: 	36569.8 is limit based on intersection of salt and background model

// RBF zone parameters
n1r=400
n2r=1000
f1r=0
f2r=0
n3r=1
f3r=0


nt=6000.0
// Could totally do 500 for testing
dt=0.001
//0.002
dsamp=10.0
//20.0
bs=25
maxYlag=0
dlag=1
kappa=0.05

// Used for the exponential dampening
alpha=100.0

pad=75
randpad=75
pctG=0.9
seed=2017

# RBF parameters
beta=0.15   #0.15 # Higher is spiky, low is smooth
trad=11 	#21   # Radius of the RBF table

