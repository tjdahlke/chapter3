include ${SEPINC}/SEP.top
main=$(shell dirname `pwd`)
project=$(shell pwd)
projbase=$(shell pwd | xargs basename)
INVname=2d-FWIup-syn
genpar=${P}/cardamom2D.p
include ${main}/ParamMakefile
include ${main}/CompileMakefile


#################################################################################

ER: ${R}/true-model-rbf-inversion.pdf ${R}/start-model-rbf-inversion.pdf

NR:

CR: ${R}/model-30-RBF-inversion.pdf ${R}/GNinversion-rbf-objfunc-0.pdf

#################################################################################

winpar=min3=214800 n3=1 min2=48000 max2=53000 min1=0 max1=2250
${R}/model-%-RBF-inversion.v:
	Window3d ${winpar} n4=1 f4=$* < ${vel_path}comp | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/true-model-rbf-inversion.v:
	Window3d ${winpar} n4=1 < truevel_unpadded.H | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/start-model-rbf-inversion.v:
	Window3d ${winpar} n4=1 < ${vel_path}comp | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/GNinversion-rbf-objfunc-%.v:
	Window3d n3=1 f3=$* < obj${INVname}.Hcomp > $@1
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $@1
	sfgraph label2="Objecive function value" label1="Iteration" title=" " < $@1 > $@
	rm $@1

#################################################################################
#		MAIN
default: ${INVname}

#################################################################################
#		WORKFLOWS

# Hessian workflow
${INVname}: ${B}/MAXBETA_RBF.x ${B}/main2d.x ${B}/APPLY_RBF2d.x ${B}/APPLY_Dop_RBF2d.x ${B}/ADD.x \
	${B}/PHI_SLOPE.x ${B}/PHI0_BUILDER_BINARY.x ${B}/BORN2d.x ${B}/Window_key ${B}/NORMALIZED_RESIDUAL.x ${B}/KINEMATIC_VSRC_OP.x \
	${souhead} ${abcs} ${recpos} ${vel_path} ${obs_path} ${guide} ${rbf_path} ${phi_path}
	python ${SCRIPTDIR}/workflow_hess_RBF2d.py ${inversion_PBS} ${Hessian_PBS} \
	inversionname=$@ levelset_iter=50 vel_path= writewave=0 nfiles=$(shell less ${recpos} | wc -l) \
	gnhess=1 hess_out_path=${pathtemphess}/hess_inv_result.H hessian=1 RBF=1 masking=0 phaseOnly=1


#################################################################################
# Make the initial velocity models

cardamom_init.H:
	Cp ${D}/cardamom_init.H $@
	# ${B}/CLEAN_SALT.x threshold=4500 < ${D}/cardamom_init.H > $@

inclusion.H: cardamom_init.H ${B}/CIRCLE2d.x
	Cp cardamom_init.H $@1
	Solver_ops file1=$@1 op=zero
	${B}/CIRCLE2d.x centz=1700.0 centy=49600.0 \
	radz=210.0 rady=200.0 replaceval=-2.0 < $@1 > $@2
	Scale < $@2 | Pad beg1=${airpad} extend=1 > $@
	rm $@1 $@2

inclusionSMALL.H: cardamom_init.H ${B}/CIRCLE2d.x
	Cp cardamom_init.H $@1
	Solver_ops file1=$@1 op=zero
	${B}/CIRCLE2d.x centz=1700.0 centy=49600.0 \
	radz=50.0 rady=50.0 replaceval=-2.0 < $@1 > $@2
	Scale < $@2 | Pad beg1=${airpad} extend=1 > $@
	rm $@1 $@2

cardamom_back.H:
	Cp ${D}/cardamom_back.H $@

# 160x400x400
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#						STARTING FILES
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# Pre-RBF Inversion (initialize the main files)

${guessphi}.noInc: cardamom_init.H ${B}/PHI0_BUILDER_BINARY.x inclusionSMALL.H
	${B}/PHI0_BUILDER_BINARY.x < $< verbose=1 thresh=${saltthresh} > $@1
	Pad beg1=${airpad} extend=1 < $@1 > $@
	rm $@1

${guessphi}.init: ${guessphi}.noInc inclusionSMALL.H
	Add scale=2,1 inclusionSMALL.H $< > $@

${guessphi}: ${guessphi}.init
	Cp $< $@

airvel.H:
	Vel n1=${airpad} n2=${ny} n3=1 d3=${dx} o3=${ox} o1=0.0 o2=0.0 d1=${dz} d2=${dx} vc=1500.0 const1=1 > $@1
	Pad end1=${nzs} < $@1 > $@
	rm $@1

${velback}: cardamom_back.H airvel.H ${guessphi}.noInc
	${B}/CLIP.x clipval=0.0 replaceval=0.0 < ${guessphi}.noInc > $@AA
	Math file1=$@AA exp='1300.0*file1' > $@1
	# Math file1=$@AA exp='800.0*file1' > $@1
	Smooth  rect1=3 rect2=3 rect3=3 < $@1 > $@2
	Pad beg1=${airpad} < cardamom_back.H > $@1
	Add $@1 airvel.H $@2 > $@
	rm $@1 $@2 $@AA

${phi_path}: ${B}/APPLY_RBF2d.x ${rbf_path} ${rbftable} ${rbfcoord} ${guessphi} ${B}/CLIP.x
	${B}/APPLY_RBF2d.x rbftable=${rbftable} rbfcoord=${rbfcoord} verbose=1 adjoint=0 \
	par=${genpar} par=${rbfpar} < ${rbf_path} > $@1
	Pad extend=0 beg1=${f1r} n1out=${nz} beg2=${f2r} n3out=${nx} beg3=${f3r} n2out=${ny} < $@1 > $@2
	Pad < ${guessphi} beg1=${f1r} n1out=${nz} beg2=${f2r} n2out=${ny} beg3=${f3r} n3out=${nx} > minus.h
	Add ${guessphi} minus.h scale=1,-1 > withHole.h
	Add withHole.h $@2 > $@
	rm $@1 $@2

${phi_path}true: ${guessphi}.noInc inclusion.H
	Scale < ${guessphi}.noInc > $@1
	Add scale=1,2 $@1 inclusion.H > $@

heaviside.H: ${guessphi}.init ${B}/APPROX_HEAVISIDE.x
	${B}/APPROX_HEAVISIDE.x kappa=${kappa} < $< > $@

heavisideTrue.H: ${phi_path}true ${B}/APPROX_HEAVISIDE.x
	${B}/APPROX_HEAVISIDE.x kappa=${kappa} < $< > $@

guessvel_unpadded.H: heaviside.H ${B}/MAKE_VEL_MODEL.x ${velback}
	${B}/MAKE_VEL_MODEL.x heaviside=heaviside.H < ${velback} > $@
	
truevel_unpadded.H: heavisideTrue.H ${B}/MAKE_VEL_MODEL.x ${velback}
	${B}/MAKE_VEL_MODEL.x heaviside=heavisideTrue.H < ${velback} > $@

${vel_path}: guessvel_unpadded.H
	Cp $< $@

make_all_randomABCs ${project}/randomABCs/randomABC_48.H: ${recpos} guessvel_unpadded.H ${B}/randomB.x ${B}/Window_key
	python ${SCRIPTDIR}/PBS_randomABCs2D.py ${recpos} ${genpar} ${project}/guessvel_unpadded.H ${project}/randomABCs ${SCRIPTDIR}/PBStemplate_M1.sh ${SCRIPTDIR} ${B} ${pad} ${randpad}

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# Parameters for trimming down the mask size for the RBF zone.
width=10
rbfwin=n1=${nzr} n2=${nyr} f1=${f1r} f2=${f2r}

${guide}: inclusion.H
	Smooth rect1=3 rect2=3  < $< | Scale > $@2
	Math file1=$@2 exp='file1*-1.0' > $@
	rm $@2

${masksalt}.init: ${guessphi}.init ${B}/PHI_SLOPE.x ${pathtempinv}/watermask.H
	${B}/PHI_SLOPE.x verbose=1 < $< | Smooth rect1=3 rect2=3 repeat=3 | Scale > $@1
	Math file1=$@1 file2=${pathtempinv}/watermask.H exp='file1*file2' > $@

${pathtempinv}/watermask.H: ${velback}
	${B}/CLIP.x clipval=${clipval} replaceval=0.0 replacelessthan=1 < $< > $@1
	${B}/CLIP.x clipval=100.0  replaceval=1.0 replacelessthan=0 < $@1 > $@ datapath=${D}/
	rm $@1

${masksalt}: ${phi_path} ${pathtempinv}/watermask.H
	python ${SCRIPTDIR}/MAKE_MASK_NEW.py binpath=${B} width=${width} infile=${phi_path} masksalt=$@ guide=${guide} watermask=${pathtempinv}/watermask.H

${rbfcoord}: ${masksalt}.init ${B}/PICK_RBF_CENTERS.x
	Math file1=${masksalt}.init file2=${guessphi}.init exp='file1+((file2+1)/2)*0.25' > $@2
	${B}/PICK_RBF_CENTERS.x < $@2 verbose=1 maxprob=0.35 minprob=0.06 \
	centers=centers.h msb=msb.h > $@
	rm $@2

${rbftable}: ${B}/RBF_BUILD_TABLE2d.x
	${B}/RBF_BUILD_TABLE2d.x par=${genpar} > $@1
	Scale rscale=0.00001 < $@1 > $@ # This makes the D^(T)D operator scale things much less than before
	rm $@1

# ${rbftable}: ${B}/RBF_BUILD_TABLE.x
# 	${B}/RBF_BUILD_TABLE.x par=${genpar} > $@1
# 	Scale rscale=0.00001 < $@1 > $@ # This makes the D^(T)D operator scale things much less than before
# 	rm $@1

${rbf_path}: ${rbftable} ${rbfcoord} ${guessphi}.init inclusionSMALL.H ${B}/APPLY_RBF2d.x ${B}/LINEARIZED_RBF2d.x
	Math file1=${guessphi}.init  exp='1000*file1' > phi_match.H
	python ${SCRIPTDIR}/rbf_inversionLIN2d.py ${RBF_params} phi_match=phi_match.H outputpath=$@1 rbfiter=${rbfiter}
	Math file1=$@1  exp='file1/1000' > $@
	rm $@1

deltaM.H: guessvel_unpadded.H truevel_unpadded.H
	Math file1=guessvel_unpadded.H file2=truevel_unpadded.H exp='file1-file2' > $@

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# Other stuff

setdatapaths:
	echo "datapath=${project}/scratch/" > ${path_out}/.datapath
	echo "datapath=${project}/scratch/" > ${project}/.datapath


single_lineUP.H:
	Cp ${D}/single_lineUP.H $@

#################################################################################
#################################################################################

# Process the REAL observed data. Injecting sources at their relative water surface location. Injecting nodes at the mirror locations.
${souhead} ${pathtempinv}/obsdata_moreKeys.H: single_lineUP.H
	Headermath maxsize=100000 \
	key1=nodeId eqn1='nodeid' \
	key2=SZ eqn2='@INT(0.5+((${airpad}+${randpad})*${dz}+sdepth)/${dz})' \
	key3=SY eqn3='@INT(0.5+(sy+${dy}*${randpad}-${oy})/${dy})' \
	key4=SX eqn4='@INT(0.5+(sx-${ox})/${dx})' \
	key5=GZ eqn5='@INT(0.5+((${airpad}+${randpad})*${dz}+gwdep)/${dz})' \
	key6=GY eqn6='@INT(0.5+(ry+${dy}*${randpad}-${oy})/${dy})' \
	key7=GX eqn7='@INT(0.5+(rx-${ox})/${dx})' \
	key8=OFFSET eqn8='offset' \
	delete_keys=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60 \
	< $< | Window3d hff=${souhead} > ${pathtempinv}/obsdata_moreKeys.H;

${recpos}: ${souhead}
	python ${SCRIPTDIR}/find_all_nodeIds.py infile=$< outfile=$@

hicut=45.0
${wavelet}:
	Wavelet phase=zero tdelay=0.2 fund=8.0 n1=${nt} wavelet=ricker2 d1=${dt} | Scale rscale=0.00001 > $@

#################################################################################

# # Make the synthetic data for the source wavefield frames used in RTM
${obs_path}: ${recpos} ${B}/main2d.x truevel_unpadded.H ${wavelet}
	python ${SCRIPTDIR}/make_all_syndata2d.py ${PBS_common} migvel=${project}/truevel_unpadded.H inversionname=$@ \
	${inversion_PBS} writesrcwave=0 nfiles=$(shell less ${recpos} | wc -l) prefix=fake_obs_data
	Cat3d virtual=1 axis=2 wrk/fake_obs_data*.H > $@ hff=$@@@

${abcs}: ${vel_path} ${B}/ABCweights2d.x
	Pad extend=1 beg1=${randpad} end1=${randpad} beg2=${randpad} end2=${randpad} < $< > $@1
	${SINGRUN} ${B}/ABCweights2d.x alpha=0.10 padT=${pad} padB=${pad} padY=${pad} < $@1 > $@
	rm $@1

#################################################################################

modelNorm0.h:
	rm -rf modelNorm.txt
	Window3d f4=0 n4=1 < ${vel_path}comp > $@1
	Add scale=1,-1 truevel_unpadded.H $@1 > $@2
	Attr want=norm param=1 < $@2 | Get parform=n norm >> modelNorm.txt
	# echo "first" >> modelNorm.txt
	rm $@1

modelNorm%.h:
	make modelNorm$(shell echo $*-1 | bc).h
	Window3d f4=$* n4=1 < ${vel_path}comp > $@1
	Add scale=1,-1 truevel_unpadded.H $@1 > $@2
	Attr want=norm param=1 < $@2 | Get parform=n norm >> modelNorm.txt
	rm $@1
	
#################################################################################

burn:
	rm -f *.h
	rm -f ${pathtempinv}/*
	rm -f ${pathtemphess}/*
	rm -f ${pathtemprbf}/*
	rm -f *.B
	rm -f *.tmp
	rm -f wrk/*
	rm -f *.Hp
	rm -f log*
	rm -f *.H*
	rm -f ${SCRIPTDIR}/*.pyc
	rm -f *.txt
	rm -f core.*
	rm -f .make.dependencies.LINUX
	rm -rf scratch/*
	rm -f *.log
	rm -rf *.p
	rm -rf *.pp


include ${SEPINC}/SEP.bottom


