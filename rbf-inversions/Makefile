main=$(shell pwd)
#include ParamMakefile
O=${main}/Obj
M=${main}/Mod
B=${main}/Bin
R=${main}/Fig
FIG=>/dev/null out=

################################################################################

default: EXE NR ER CR 

EXE:
	make --directory=2dFWIupSYN $@

NR:

ER: ${R}/true-model-rbf-inversion.pdf ${R}/start-model-rbf-inversion.pdf

CR: ${R}/model-30-RBF-inversion.pdf ${R}/GNinversion-rbf-objfunc-0.pdf ${R}/model-30-noRBF-inversion.pdf ${R}/GNinversion-norbf-objfunc-0.pdf

################################################################################

workflowrun:
	make --directory=2dFWIupSYN $@
	make --directory=2dFWIupSYNnorbf $@

#################################################################################
#	FIGURES

winpar=min3=214800 n3=1 min2=48000 max2=53000 min1=0 max1=2250

# With RBF
${R}/true-model-rbf-inversion.v:
	Window3d ${winpar} n4=1 < ${main}/2dFWIupSYN/truevel_unpadded.H | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label1='Z[m]' label2='Crossline Y[m]' label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/start-model-rbf-inversion.v:
	Window3d ${winpar} n4=1 < ${main}/2dFWIupSYN/tmp/tmp_inv/vel_full.Hcomp | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label1='Z[m]' label2='Crossline Y[m]' label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/model-%-RBF-inversion.v:
	Window3d ${winpar} n4=1 f4=$* < ${main}/2dFWIupSYN/tmp/tmp_inv/vel_full.Hcomp | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label1='Z[m]' label2='Crossline Y[m]' label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/GNinversion-rbf-objfunc-%.v:
	Window3d n3=1 f3=$* < ${main}/2dFWIupSYN/obj2d-FWIup-syn.Hcomp > $@1
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $@1
	sfgraph label2="Objective function value" label1="Iteration" title=" " < $@1 > $@
	rm $@1

${R}/fat-GNinversion-rbf-objfunc-%.v:
	Window3d n3=1 f3=$* < ${main}/2dFWIupSYN/obj2d-FWIup-syn.Hcomp > $@1
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $@1
	sfgraph axisfat=5,5 plotfat=10 label2="Objective function value" label1="Iteration" title=" " < $@1 > $@
	rm $@1

# No RBF
${R}/model-%-noRBF-inversion.v:
	Window3d ${winpar} n4=1 f4=$* < ${main}/2dFWIupSYNnorbf/tmp/tmp_inv/vel_full.Hcomp | Grey gainpanel=a color=jc newclip=1 wantscalebar=y label1='Z[m]' label2='Crossline Y[m]' label3='Velocity [m/s]' title=" " ${FIG}$@

${R}/GNinversion-norbf-objfunc-%.v:
	Window3d n3=1 f3=$* < ${main}/2dFWIupSYNnorbf/objNORBF-2d-FWIup-syn.Hcomp > $@1
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $@1
	sfgraph label2="Objective function value" label1="Iteration" title=" " < $@1 > $@
	rm $@1

${R}/fat-GNinversion-norbf-objfunc-%.v:
	Window3d n3=1 f3=$* < ${main}/2dFWIupSYNnorbf/obj2d-FWIup-syn.Hcomp > $@1
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $@1
	sfgraph axisfat=5,5 plotfat=10 label2="Objective function value" label1="Iteration" title=" " < $@1 > $@
	rm $@1
#-------------------------------------------------------------------------------
#		RULES FOR BUILDING PDF files

%.pdf: %.v
	vplot2eps color=y $< $*.ps
	ps2pdf -dEPSCrop -dAutoFilterColorImages=false  -dColorImageFilter=/FlateEncode  -dAutoFilterGrayImages=false  -dGrayImageFilter=/FlateEncode  -dAutoFilterMonoImages=false  -dMonoImageFilter=/CCITTFaxEncode $*.ps $@
	rm $*.ps

################################################################################

clean: jclean

wipe:
	rm -f ${R}/*.pdf
	rm -f ${R}/*.ps
	rm -f ${R}/*.v*

burn: clean wipe
	rm -f ${B}/*
	rm -f ${O}/*
	rm -f ${M}/*
	rm -f .make.dependencies.LINUX

################################################################################
