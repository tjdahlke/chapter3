title="C33(Stiffness:GPa)"
in="../VELOCITY/sediment_0_C33.H@"
n1=1 n2=1 n3=1 n4=1 n5=1 n6=1 n7=1
o1=0 o2=0 o3=0 o4=0 o5=0 o6=0 o7=0
d1=1 d2=1 d3=1 d4=1 d5=1 d6=1 d7=1
esize=4 data_format=xdr_float n1=151
o1=0
d1=100
label1="Z(Depth:meter)"
Unit_Value1=1
esize=4 data_format=xdr_float n2=212
o2=195150
d2=200
label2="X(Location:meter)"
Unit_Value2=1
esize=4 data_format=xdr_float n3=183
o3=34410
d3=180
label3="Y(Location:meter)"
Unit_Value3=1
Attribute_Unit_Value=1e+09
Undefined_Value=-1
in=/net/brick5/data2/Cardamom/VELOCITY/sediment_0_C33.H@


/opt/SEP/SEP7.0/bin/Math:   taylor@fantastic.stanford.edu   Thu Dec  7 13:24:25 2017
		sets next: in="/scratch1/taylor/ver_Pwave_vel.H@"
		esize=4
	#Using @SQRT(1000000000*file1/file2) to produce real data

		#file1="/net/brick5/data2/Cardamom/VELOCITY/sediment_0_C33.H"
		#file2="/net/brick5/data2/Cardamom/VELOCITY/sediment_0_dens.H"
		data_format="xdr_float"


/opt/SEP/SEP7.0/bin/Math:   taylor@fantastic.stanford.edu   Thu Dec  7 13:24:36 2017
		sets next: in="/scratch1/taylor/Pwave.H@"
		esize=4
	#Using 0.6*file1+0.4*file2 to produce real data

		#file1="ver_Pwave_vel.H"
		#file2="hor_Pwave_vel.H"
		data_format="xdr_float"
title='Blended Pwave Velocity [m/s]'
in=/data/sep/tjdahlke/research/Dat/Pwave.H@


Window3d:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:06:15 2018
		sets next: in="/data/sep/tjdahlke/scratch/research/Window3d@zh2nMX"
#Window #f1=0  #j1=1 #n1=50
#Window #f2=97  #j2=1 #n2=3
#Window #f3=57  #j3=1 #n3=85
		junkME=0
	n2=3  o2=214550.000000  d2=200.000000   label2="X(Location:meter)"   unit2="Undefined"
	n3=85  o3=44670.000000  d3=180.000000   label3="Y(Location:meter)"   unit3="Undefined"
		gff="-1"
		hff="-1"
hff=-1 gff=-1  n4=1  n5=1  n6=1  n7=1  n8=1  n9=1 
	n1=50  o1=0.000000  d1=100.000000   label1="Z(Depth:meter)"   unit1="Undefined"
		hff="-1"
		gff="-1"
		esize=4
		data_format="xdr_float"



Interp:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:06:15 2018
		sets next: in="/data/sep/tjdahlke/scratch/research/Interp@3i1GKf"
	sinc interpolation with 10 points
	n1=491 o1=0.000000 d1=10.000000
	n2=16 o2=214800.000000 d2=10.000000
	n3=1479 o3=45010.000000 d3=10.000000
		data_format="xdr_float"



Window3d:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:06:59 2018
		sets next: in="/data/sep/tjdahlke/scratch/research/Window3d@Uz4cJL"
#Window #f1=0  #j1=1 #n1=491
#Window #f2=0  #j2=1 #n2=1
#Window #f3=0  #j3=1 #n3=1479
		junkME=0
	n2=1479  o2=45010.000000  d2=10.000000   label2="Y(Location:meter)"   unit2="Undefined"
	n3=1  o3=214800.000000  d3=10.000000   label3="Y(Location:meter)"   unit3="Undefined"
		gff="-1"
		hff="-1"
hff=-1 gff=-1  n4=1  n5=1  n6=1  n7=1  n8=1  n9=1 
	n1=491  o1=0.000000  d1=10.000000   label1="Z(Depth:meter)"   unit1="Undefined"
		hff="-1"
		gff="-1"
		esize=4
		data_format="xdr_float"



/data/sep/tjdahlke/thesis/chapter3/rbf-inversions/Bin/CLIP.x:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:06:59 2018
		sets next: in="/data/sep/tjdahlke/thesis/chapter3/rbf-inversions/Dat/CLIP.x@QFV6a8"
		From par: verbose=no
		From par: clipval=1560
		From par: replacelessthan=yes
		From par: replaceval=1500
		From par: range=no
		data_format="xdr_float"


/data/sep/tjdahlke/thesis/chapter3/rbf-inversions/Bin/CLIP.x:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:07:00 2018
		sets next: in="/data/sep/tjdahlke/thesis/chapter3/rbf-inversions/Dat/CLIP.x@kqBrOh"
		From par: verbose=yes
		From par: clipval=1.5
		From par: replacelessthan=yes
		From par: replaceval=1750
		From par: range=yes
		From par: lowR=1560
		From par: highR=1750
		data_format="xdr_float"


Window3d:   tjdahlke@cees-rcf.stanford.edu   Wed Dec 26 17:07:00 2018
		sets next: in="../Dat/cardamom_back.H@"
#Window #f1=0  #j1=1 #n1=295
#Window #f2=0  #j2=1 #n2=1000
#Window #f3=0  #j3=1 #n3=1
		junkME=0
	n2=1000  o2=45010.000000  d2=10.000000   label2="Y(Location:meter)"   unit2="Undefined"
	n3=1  o3=214800.000000  d3=10.000000   label3="Y(Location:meter)"   unit3="Undefined"
		gff="-1"
		hff="-1"
hff=-1 gff=-1  n4=1  n5=1  n6=1  n7=1  n8=1  n9=1 
	n1=295  o1=0.000000  d1=10.000000   label1="Z(Depth:meter)"   unit1="Undefined"
		hff="-1"
		gff="-1"
		esize=4
		data_format="xdr_float"

