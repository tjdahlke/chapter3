

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\chapter{Radial basis functions for model sparsity}
\label{chap:chap3}


\par In the previous chapter, I reviewed the advantages of using the inverse Hessian to improve the search direction used in the FWI problem. In this chapter, I first discuss how re-parameterizing the implicit surface to a sparse domain with Radial Basis Functions (RBFs) may improve the theoretical convergence rate of the Hessian inversion. Next, I demonstrate how RBFs can give a satisfactory representation of a dense implicit surface when suitable parameters are chosen. Last, I give a comparison of level set inversions showcasing the improved convergence rate that is gained by using RBFs. 

        
\section{Motivation}

\par Any uniform increase in the 3D model size increases the number of model parameters by $O(n_{x}n_{y}n_{z})$. When using a Newton method as described in equation \ref{eq:basicNewton}, we make use of a Hessian that (in matrix form) has $(n_{x}n_{y}n_{z})^{2}$ elements. As the model expands to 3D, this quickly becomes intractable to store in memory or even on disk. In practice, we overwhelmingly prefer to solve the inverse Hessian system (equation \ref{eq:basicNewton}) using the conjugate gradient method, which only requires forward applications of the Hessian operator (in our case, the Gauss-Newton Hessian). Regardless, the upper limit of the number of iterations needed to converge fully using the conjugate-gradient method is inversely proportional to the number of model parameters \citep{aster2011parameter}. As such, our desire to improve the rate of convergence when solving the Newton system motivates us to reduce the number of model parameters in some manner.


\par For 3D seismic wave propagation, the coarseness of spatial sampling is based on constraints imposed by the numerical dispersion and numerical stability inherent in our finite differencing scheme. For the purpose of wave modeling, our spatial resolution will generally be higher than the features we can robustly resolve from the data. We can typically use uniform down-sampling methods on the update gradient to create a smaller model space without significantly affecting our end results \citep{cox}. One of the disadvantages of using down-sampling to reduce the model space is that it uniformly reduces resolution. This runs counter to our interest in having high resolution in specific areas of interest, like the salt boundaries. For this reason, any application of down-sampling usually gives less than satisfactory results for inversion resolution.

In the level set problem, we are most interested in areas around and within the salt boundaries. The implicit surface tracking our salt boundary doesn't need to be represented by a fine grid across the whole domain like the one used for wave propagation. However, aggressive down-sampling schemes will quickly start to deteriorate the information that we wish to keep. What is needed is a method that allows the flexibility of spatially varying resolution.


\section{Radial basis function implementation}

\par One way to achieve model space reduction is with a basis function that lets us use fewer model parameters to describe a larger spatial area. Radial basis functions are a simple and effective kernel to use. We can separately scale and then sum an aggregation of RBFs to approximate a dense implicit surface, with the approximation accuracy related to the shape of the RBF kernel and the number of RBF kernels used. \cite{kadu2017} propose an implementation like this which replaces a dense Cartesian grid parametrization of the implicit surface $\boldsymbol{\phi}$ with a surface described as an aggregate of evenly spaced RBFs:

\begin{equation}
\boldsymbol{\phi}(\boldsymbol{\lambda}; \epsilon) = \sum_{i}^{N_{\lambda}} \lambda_{i} \exp^{-(\epsilon \mathbf{r}_{i})^2}.
\label{eq:rbf2phi}
\end{equation}

\noindent In this formulation, $\boldsymbol{\lambda}$ is the new (sparse) model parameter vector, $N_{\lambda}$ is the length of $\boldsymbol{\lambda}$, $\mathbf{r}_{i}$ is an array of radial distance from the $i$th RBF center with size $N_{z} \cdot N_{x} \cdot N_{y}$ for the 3D spatial case, and $\epsilon$ controls the sharpness of the RBF taper (constant for all $i$). In their approach, \cite{kadu2017} chose a uniform distance between each RBF center location beforehand based on the resolution they desired.

\par However, there are only a few areas where one really wants high resolution (namely, the salt edge where we expect boundary movement). The resolution achieved with the sparse parametrization is primarily based on how many RBFs are used to describe a particular area (RBF density). Because the salt evolves from an initial boundary, the further from this initial boundary a model region is, the less likely that it will be updated, which means low RBF density can be justified. On the other hand, regions close to the current salt boundary are more likely to update, justifying higher RBF density. Therefore, I introduce the idea of spatially varying the density of the RBF centers to represent the implicit surface in a sparse fashion (as shown in Figure \ref{fig:centers-dist}). This allows clustering the center locations of the RBFs in areas I expect to see updating occur, while using a lower density in regions where I don't expect updating (see Figure \ref{fig:centers}). This is a more efficient way to distribute RBF positions, resulting in fewer RBF parameters needed to attain high resolution around the salt boundary than if I used the regular RBF spacing described in \cite{kadu2017}.

% RBF centers distribution
\plot{centers-dist}{width=5in}{The probability distribution used to randomly position the radial basis function centers in Figure \ref{fig:centers}. \ER}

% RBF centers positions 
\plot{centers}{width=5in}{Center positions for radial basis functions used to construct the implicit surface.\ER}

\par For the new representation of $\boldsymbol{\phi}$ described in equation \ref{eq:rbf2phi}, the operator $\mathbf{D}$ must be modified to account for the additional linear transformation:
\begin{align*}
  \mathbf{D} &= \begin{bmatrix}   \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \boldsymbol{\phi}}
  \frac{\partial \boldsymbol{\phi}}{\partial \boldsymbol{\lambda}}  &    \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \mathbf{b}} \end{bmatrix} \\
  &= \begin{bmatrix}
  \widetilde{\delta}(\boldsymbol{\phi_{o}})(\mathbf{c_{ s } - b}) \exp^{-(\epsilon \mathbf{r})^2} & \quad
  \mathbf{I} - \widetilde{H}(\boldsymbol{\phi_{o}})
  \end{bmatrix}.
  \stepcounter{equation}\tag{\theequation}\label{eq:Dop1rbf}\
\end{align*}

\noindent In this formulation, $\widetilde{H}$ is the Heaviside approximation (equation \ref{eq:heaviapproxCompact}), $\mathbf{I}$ is the identity matrix, $\widetilde{\delta}$ is the derivative of $\widetilde{H}$, $\mathbf{r}$ is a tensor of size $N_{z} \cdot N_{x} \cdot N_{y} \cdot N_{\lambda}$ for the 3D spatial case, $\mathbf{b}$ is the background velocity ($\mathbf{b_{0}}$ is fixed), $\boldsymbol{\phi}$ is the implicit surface ($\boldsymbol{\phi_{0}}$ is fixed), and $\mathbf{c_{s}}$ is the constant salt velocity. Further, the model space has also changed:

\begin{align*}
  \triangle \mathbf{p} = \begin{bmatrix}  \triangle \boldsymbol{\lambda}  \\ \triangle \mathbf{b} \end{bmatrix}.
\end{align*}

\noindent When I apply the operator $\mathbf{D}$ or $\mathbf{D^{T}}$, I consider the locations of the RBF centers to be fixed throughout the inversion (for example, as shown in Figure \ref{fig:centers}).

\subsection{Computational considerations}
\par The forward application of the $\mathbf{D}$ operator derived earlier now includes a $\exp^{-(\epsilon \mathbf{r})^2}$ term. This means that for each element $\lambda_{i}$ in the model vector, I scale and sum a Gaussian function to the aggregated surface, $\boldsymbol{\phi}$. If I compute $\exp^{-(\epsilon \mathbf{r})^2}$ over the full model, then the $\mathbf{D}$ operator becomes expensive to apply on a large 3D spatial domain, since the algorithm would loop over the full model space for each RBF. One observation that I leverage is that the value of the radial basis function decreases significantly at high values of $r$. Furthermore, I scale and sum the same Gaussian each time ($\epsilon$ is fixed). Based on this, I pre-compute the radial basis function $\exp^{-(\epsilon \mathbf{r})^2}$ just once over a region where its value is actually significant. Naturally, the size of this region is based on the taper of the RBF, which is governed by $\epsilon$. By choosing $\epsilon$ well, and then pre-computing the corresponding Gaussian function over a limited region, the RBF summation computation is greatly simplified, and increases the speed of applying $\mathbf{D}$ or $\mathbf{D^{T}}$ significantly.



\section{Examples of RBF fitting}
\par In order to show how RBFs can accurately represent a salt body with far fewer parameters than a dense representation, I demonstrate their use on a Gulf of Mexico velocity model provided by Shell. I choose a section of the velocity model that has a notable salt protrusion in it as an example (Figure \ref{fig:both}). Beginning with this model, I build a probability density map that favors placing RBF centers near the original picked boundary (Figure \ref{fig:centers-dist}), with less likelihood further from that boundary. From this, I generate random RBF positions (Figure \ref{fig:centers}). Using these RBF centers, I then perform a non-linear conjugate gradient inversion to find the proper weighting of the RBF kernels in order to best fit the starting model salt shape (Figure \ref{fig:matching-phi-Heavi}), which is built from an initial implicit surface (Figure \ref{fig:matching-phi}). The inversion finds sparse parameters that create a new implicit surface (Figure \ref{fig:resulting-phi}), which looks different from the one used to build the fitting model (Figure \ref{fig:matching-phi}). However, when I apply the Heaviside function to either of these to create the salt, we can see that the sparse parameters create a model with a good fit (compare Figures \ref{fig:matching-phi-Heavi} and \ref{fig:resulting-phi-Heavi}).
The inversion converges relatively quickly (Figure \ref{fig:objfunc}), but naturally has some unresolved residual since the RBF parametrization is sparse and cannot match the dense salt model guess perfectly (see Figure \ref{fig:rbfinv-diff-full}).

% RTM overlay
\plot{both}{width=5in}{Salt model used by Shell (white) overlaid on the corresponding RTM image.\ER}

% Phi and Heaviside results start
\multiplot{2}{matching-phi,matching-phi-Heavi}{width=0.75\columnwidth}{a) Implicit surface $\boldsymbol{\phi_{\text{o}}}$ that creates the salt body shape (b) that my inversion tries to match. b) is the result of applying the Heaviside function to $\boldsymbol{\phi_{\text{o}}}$.}

% Phi and Heaviside results inversion result
\multiplot{2}{resulting-phi,resulting-phi-Heavi}{width=0.75\columnwidth}{a) Implicit surface $\boldsymbol{\phi_{\text{final}}}$ created from final inverted RBF parameters ($\boldsymbol{\phi}_{\text{final}}=\mathbf{D}\boldsymbol{\lambda}_{\text{final}}$); b) the result of applying the Heaviside function to $\boldsymbol{\phi_{\text{final}}}$.}


Figure \ref{fig:rbfinv-diff-full} shows that the matching model and the resulting inverted model after Heaviside function application are quite similar. However, if we choose $\epsilon$ and the corresponding RBF footprint poorly, we wont be able to represent the original salt model as well as we could. When $\epsilon$ is too high, the RBF decays quickly, resulting in a model that is less smooth. Alternatively, when $\epsilon$ is too low, the RBF decays slowly and creates a model that is too smooth. Figure \ref{fig:rbfinv-diff-sparse} shows a case where these parameters were chosen poorly, while Figure \ref{fig:rbfinv-diff-full} does a much better job in both the salt center region as well as the boundary. 

% RBF objective function
\plot{objfunc}{width=5in}{Log of normalized objective function from the non-linear inversion used to find the RBF parameters used in Figures \ref{fig:resulting-phi}, \ref{fig:resulting-phi-Heavi} and \ref{fig:rbfinv-diff-full}.\ER}


% Differences in epsilon parameter
\multiplot{2}{rbfinv-diff-full,rbfinv-diff-sparse}{width=0.75\columnwidth}{Differences between original salt model and the resulting model produced by the RBF representation. (a) shows difference of fitted salt model using $\epsilon=0.25$ value, while (b) shows difference of fitted salt model using $\epsilon=2.25$ value. Both cases used 98\% fewer model parameters than the original full-grid scheme, and both used the same RBF positions. Background velocity is 2.5 km/s and salt velocity is 4.5 km/s.\ER}




\section{Comparison of sparse (RBF) and dense model inversions}

\par In order to illustrate the improved rate of convergence gained by using a sparse RBF model, I demonstrate on a 2D synthetic inversion example. The goal here is to show how RBFs affect the greater shape optimization problem by comparing the inversion convergence rates between a sparse RBF parametrization and a dense non-RBF parametrization. 

\plot{true-model-rbf-inversion}{width=5in}{True velocity model that was used to synthetically generate the `observed' data. \ER}

\plot{start-model-rbf-inversion}{width=5in}{Starting velocity model. Note the inclusion is much smaller than in the true model (Figure \ref{fig:true-model-rbf-inversion}). \ER}


I choose a `true' model similar to Figure \ref{fig:both} that I wish to invert for (Figure \ref{fig:true-model-rbf-inversion}) that has an inclusion close to the edge. This model is chosen to show how the RBF inversion can invert for a more unusual model geometry. I begin with a model containing a much smaller inclusion (Figure \ref{fig:start-model-rbf-inversion}). Just as in chapter two (Algorithm \ref{alg:steepest-descent}), the inversion workflow has an outer loop where I do non-linear modeling to find the residual and use the adjoint Born operator to find the gradient. However, following this I now have an inner loop using iterative methods to invert the Hessian and find the search direction from the gradient (solving equation \ref{eq:basicNewton}), before performing the linesearch. In these examples, the inner loop uses a Gauss-Newton Hessian, and I compare using a dense model (no RBF parametrization) to using a sparse model (with RBF parametrization). The RBF model has only 7$\%$ of the model points that the dense model has. I perform the same number of iterations (20) for each inner-loop linear inversion of the Gauss-Newton Hessian using a conjugate gradient solver. For the first Hessian inversion, the sparse RBF parameterization (Figure \ref{fig:GNinversion-rbf-objfunc-0}) converges faster than the dense model example (Figure \ref{fig:GNinversion-norbf-objfunc-0}). Note that the objective function curves become negative since I use a conjugate gradient (CG) solver based on equation \ref{eq:cg}:


\begin{equation}
  \min \psi(\mathbf{\triangle m}) = \frac{1}{2} \mathbf{\triangle m^{T} H \triangle m - g^{T} \triangle m},
\label{eq:cg}
\end{equation}



\noindent instead of a conjugate gradient least-squares (CGLS) solver (based on equation \ref{eq:cgls}):

\begin{equation}
  \min \psi(\mathbf{\triangle m}) = \frac{1}{2}|| \mathbf{H \triangle m - g}||^{2}_{2}.
\label{eq:cgls}
\end{equation}

\noindent Because the Hessian is a symmetric operator, I can take advantage of using the CG solver instead of the more expensive CGLS solver. However, the residual is not squared as in CGLS, so the objective function can take negative values.



\plot{GNinversion-rbf-objfunc-0}{width=4in}{Objective function from the first inner-loop Gauss Newton inversion of the sparse (RBF) model system. \CR}

\plot{GNinversion-norbf-objfunc-0}{width=4in}{Objective function from the first inner-loop Gauss Newton inversion of the dense (non-RBF) model system. \CR} %Note the values are more negative than in Figure \ref{fig:GNinversion-rbf-objfunc-0}, which is due to the model space having more parameters.

\par I find that after 14 outer loop (non-linear) iterations the sparse (RBF) inversion converges, while the dense (non-RBF) inversion objective function is still descending after 40 iterations (Figure \ref{fig:RBF-vs-noRBF-dataNorm}). The normalized model norm for the sparse inversion also reaches a lower value (Figure \ref{fig:RBF-vs-noRBF-modelNorm}), while the non-RBF inversion actually increases instead. By representing the dense model sparsely with RBFs, I reduce the number of parameters as well as create a smoother equivalent update in the dense model space (with smoothness based on the $\epsilon$ used). In this sense, the RBFs act somewhat like a regularization. Both these factors contribute to the improved convergence rate that I find when using RBFs in the inversion. When comparing the inverted model results of each parametrization, one can clearly see that the RBF approach (Figure \ref{fig:model-30-RBF-inversion}) provides a superior result to the dense model space parametrization (Figure \ref{fig:model-30-noRBF-inversion}).

\plot{RBF-vs-noRBF-dataNorm}{width=5in}{Objective function (data norm) comparison of the outer-loop inversion for RBF and non-RBF parameterized model approaches. \CR}

\plot{RBF-vs-noRBF-modelNorm}{width=5in}{Model norm comparison of the outer-loop inversion for RBF and non-RBF parameterized model approaches. \CR}

\plot{model-30-RBF-inversion}{width=5in}{Velocity model after 30 outer loop (non-linear) iterations using RBF parametrization.}

\plot{model-30-noRBF-inversion}{width=5in}{Velocity model after 30 outer loop (non-linear) iterations without RBF parametrization.}


\section{Conclusions}

\par The speed at which one can invert the Hessian system and find the search direction is sensitive to the number of parameters in the model, and using 3D spatial models requires a large number of model parameters for wave propagation. However, I can sparsely represent this dense model using radial basis functions to achieve significant parameter reduction. I show that this representation allows me to accurately depict the dense model, and that steps can be taken to make this transform computationally efficient. Finally, when I compare inversions using a sparse representation (RBFs) versus a dense representation, I find that the sparse model inversion provides a better outcome and a faster convergence rate.















