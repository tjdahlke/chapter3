include makefile.top

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#		FIGURES
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

default: EXE NR ER CR

EXE: ${B}/APPLY_RBF.x  ${B}/LINEARIZED_RBF.x  ${B}/CLIP.x ${B}/NONLINEAR_RBF_PHI.x ${B}/RBF_BUILD_TABLE.x ${B}/NONLINEAR_RBF_PHI.x ${B}/PHI0_BUILDER_BINARY.x ${B}/MAKE_MASK_RBF.x ${B}/PICK_RBF_CENTERS.x
	make initialize

NR: 

ER: ${R}/rtm.pdf ${R}/both.pdf ${R}/centers.pdf ${R}/centers-dist.pdf ${R}/matching-phi.pdf ${R}/matching-phi-Heavi.pdf ${R}/resulting-phi.pdf ${R}/resulting-phi-Heavi.pdf ${R}/rbfinv-diff-full.pdf ${R}/rbfinv-diff-sparse.pdf ${R}/objfunc.pdf 

CR:

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

${R}/rtm.v:
	< ${D}/rtm.H Grey title=" " gainpanel=e out=$@ >/dev/null

${R}/both.v:
	Scale < ${D}/rtm.H > $@1
	Scale < ${D}/cardamom_init.H > $@2
	Math file1=$@1 file2=$@2 exp='file1+0.001*file2' | Grey title=" " gainpanel=e out=$@ >/dev/null

${R}/centers.v: centers.H
	echo "label1='Z[m]' label2='Crossline Y[m]'" >> centers.H
	Window3d < centers.H > $@1
	Grey title=" " < $@1 out=$@ >/dev/null
	rm $@1

${R}/centers-dist.v: centers-dist.H
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Probability of RBF occurring'" >> centers-dist.H
	Window3d  < centers-dist.H > $@1
	Grey pclip=100 color=jc title=" " wantscalebar=y < $@1 out=$@ >/dev/null
	rm $@1

${R}/matching-phi.v: ${guessphi}
	Cp $< $@1
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Implicit surface value'" >> $@1
	Grey pclip=100 color=F wantscalebar=y title=" " < $@1 out=$@ >/dev/null
	rm $@1

${R}/matching-phi-Heavi.v: ${guessphi} ${B}/CLIP.x
	${B}/CLIP.x clipval=0.0 replaceval=0.0 replacelessthan=1 < ${guessphi} > $@1
	${B}/CLIP.x clipval=0.0 replaceval=1.0 replacelessthan=0 < $@1 > $@2
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Magnitude'" >> $@2
	Grey pclip=100 color=jc wantscalebar=y title=" " < $@2 out=$@ >/dev/null
	rm $@1 $@2

${R}/resulting-phi.v: ${B}/APPLY_RBF.x ${rbftable} ${rbfcoord} ${rbf_path} ${guessphi}
	${B}/APPLY_RBF.x rbftable=${rbftable} rbfcoord=${rbfcoord} \
	verbose=1 par=${genpar} par=${rbfpar} < ${rbf_path} > $@1
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Implicit surface value'" >> $@1
	Grey pclip=100 color=F wantscalebar=y title=" " < $@1 out=$@ >/dev/null
	rm $@1

${R}/resulting-phi-Heavi.v: ${B}/APPLY_RBF.x ${rbftable} ${rbfcoord} ${rbf_path} ${guessphi}
	${B}/APPLY_RBF.x rbftable=${rbftable} rbfcoord=${rbfcoord} \
	verbose=1 par=${genpar} par=${rbfpar} < ${rbf_path} > $@1
	${B}/CLIP.x clipval=0.0 replaceval=0.0 replacelessthan=1 < $@1 > $@2
	${B}/CLIP.x clipval=0.0 replaceval=1.0  replacelessthan=0 < $@2 > $@3
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Magnitude'" >> $@3
	Grey pclip=100 color=jc wantscalebar=y title=" " < $@3 out=$@ >/dev/null
	rm $@1 $@2 $@3

${R}/rbfinv-diff-full.v: ${B}/NONLINEAR_RBF_PHI.x ${rbftable} ${rbfcoord} ${rbf_path} ${guessphi}
	${B}/NONLINEAR_RBF_PHI.x rbftable=${rbftable} rbfcoord=${rbfcoord} \
	verbose=1 par=${genpar} par=${rbfpar} < ${rbf_path} > $@1
	Add scale=1000,-1000 ${guessphi} $@1 > $@2
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Velocity difference [m/s]'" >> $@2
	Grey pclip=100 wantscalebar=y title=" " < $@2 out=$@ >/dev/null
	rm $@1 $@2

${R}/rbfinv-diff-sparse.v: ${B}/NONLINEAR_RBF_PHI.x ${rbftable}2 ${rbfcoord} ${rbf_path}2 ${guessphi}
	${B}/NONLINEAR_RBF_PHI.x rbftable=${rbftable}2 rbfcoord=${rbfcoord} \
	verbose=1 par=${genpar} par=${rbfpar} < ${rbf_path}2 > $@1
	Add scale=1000,-1000 ${guessphi} $@1 > $@2
	echo "label1='Z[m]' label2='Crossline Y[m]' label3='Velocity difference [m/s]'" >> $@2
	Grey pclip=100 wantscalebar=y title=" " < $@2 out=$@ >/dev/null
	rm $@1 $@2

${R}/objfunc.v: objgoodEPS.H
	echo "d2=1.0 d3=1.0 d4=1.0 d5=1.0 d6=1.0 d7=1.0 unit1= unit2= " >> $<
	Scale < $< | Log centered=0 > tmp.h  
	sfgraph label2="LOG( objective function value )" label1="Iteration" title=" " < tmp.h >$@
	rm tmp.h

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#						CARDAMOM RBF FITTING INVERSION
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

${guessphi}: ${D}/cardamom_init.H ${B}/PHI0_BUILDER_BINARY.x
	${B}/PHI0_BUILDER_BINARY.x < $< thresh=${saltthresh} phi_out=$@ &>logphi;

${masksalt}: ${guessphi} ${B}/MAKE_MASK_RBF.x
	${B}/MAKE_MASK_RBF.x < $< masksalt=${masksalt}

${rbfcoord} centers.H centers-dist.H: ${guessphi} ${B}/PICK_RBF_CENTERS.x
	${B}/PICK_RBF_CENTERS.x < $< verbose=1 maxprob=0.35 minprob=0.01 smoothrad=2 \
	smoothpass=20 centers=centers.H msb=centers-dist.H > ${rbfcoord}

#-------------------------------------------------
${rbftable}: ${B}/RBF_BUILD_TABLE.x
	${B}/RBF_BUILD_TABLE.x par=${genpar} beta=0.25 trad=20 > $@1
	Scale rscale=0.00001 < $@1 > $@ # This makes the D^(T)D operator scale things much less than before
	rm $@1

${rbf_path} objgoodEPS.H: ${guessphi} ${rbftable} ${rbfcoord} ${B}/APPLY_RBF.x  ${B}/LINEARIZED_RBF.x  ${B}/NONLINEAR_RBF_PHI.x
	PYTHONPATH=${PYPATH} python ${main}/rbf_inversionNL_PHI.py ${RBF_params} rbftable=${rbftable}  \
	invname=goodEPS phi_match=${guessphi} rbf_path=${rbf_path}

#-------------------------------------------------
${rbftable}2: ${B}/RBF_BUILD_TABLE.x
	${B}/RBF_BUILD_TABLE.x par=${genpar} beta=2.25 trad=20 > $@1
	Scale rscale=0.00001 < $@1 > $@ # This makes the D^(T)D operator scale things much less than before
	rm $@1

${rbf_path}2 objbadEPS.H: ${guessphi} ${rbftable}2 ${rbfcoord} ${B}/APPLY_RBF.x  ${B}/LINEARIZED_RBF.x  ${B}/NONLINEAR_RBF_PHI.x
	PYTHONPATH=${PYPATH} python ${main}/rbf_inversionNL_PHI.py ${RBF_params} rbftable=${rbftable}2 \
	invname=badEPS phi_match=${guessphi} rbf_path=${rbf_path}2

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

initialize:
	# git submodule update --init --recursive;
	# cd generic-solvers-1.0; make install;
	echo "datapath=${main}/scratch/" > ./.datapath;

#-------------------------------------------------------------------------------
#		RULE FOR BUILDING PDF files

%.pdf: %.v
	vplot2eps color=y $< $*.ps
	ps2pdf -dEPSCrop -dAutoFilterColorImages=false  -dColorImageFilter=/FlateEncode  -dAutoFilterGrayImages=false  -dGrayImageFilter=/FlateEncode  -dAutoFilterMonoImages=false  -dMonoImageFilter=/CCITTFaxEncode $*.ps $@
	rm $*.ps

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

include ${SEPINC}/SEP.bottom
