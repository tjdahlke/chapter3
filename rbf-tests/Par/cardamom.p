# Velocity Geometry
n1=325
n2=233
d1=10
d2=30
n3=1
o1=0.0
o2=47009.8
d3=25
vc=2000.0

# RBF parameters
beta=0.15   	# Higher is spiky, low is smooth
trad=20 		# Radius of the RBF table # trad = SQRT(-LOG(threshold)/(beta**2))
