
program PHI0_BUILDER_BINARY
use sep
implicit none

!===============================================================
real, dimension(:,:), allocatable				:: phi0R, saltR
double precision, dimension(:,:), allocatable	:: phi0, salt
integer											:: nz, nx, iz, ix
real											:: rad, dist, a, dz, dx, oz, ox, dz1, dx1
real											:: oz1, ox1, xx, zz, cz, cx, thresh
logical                   						:: slowsalt


! Allocate the internal arrays / dimensions
 call sep_init()
 call from_history("n1",nz)
 call from_history("n2",nx)
 call from_history("o1", oz)
 call from_history("d1", dz)
 call from_history("o2", ox)
 call from_history("d2", dx)
 call from_param("thresh", thresh,0.0)
 call from_param("slowsalt", slowsalt,.false.)

 allocate(phi0(nz,nx), salt(nz,nx), phi0R(nz,nx), saltR(nz,nx))
 call sep_read(saltR)
 salt = DBLE(saltR)

! Build the initial phi surface
phi0 = -1.0
do ix=1,nx
	do iz=1,nz
		if (salt(iz,ix)>thresh) then
			phi0(iz,ix) = 1.0
		end if
	end do
end do

if (slowsalt) then
  phi0=-1.0*phi0
endif

! Output the initial levelset
if (exist_file('phi_out')) then
	phi0R = SNGL(phi0)
	call sep_write(phi0R,'phi_out')
	call to_history("n1", nz,'phi_out')
	call to_history("o1", oz,'phi_out')
	call to_history("d1", dz,'phi_out')
	call to_history("n2", nx,'phi_out')
	call to_history("o2", ox,'phi_out')
	call to_history("d2", dx,'phi_out')
	call to_history("label1", 'z','phi_out')
	call to_history("label2", 'x','phi_out')
else
	write(0,*) "You need to provide an outside levelset output called 'phi_out'"
end if


end program
