
!################################################################################
!
!		MASKING PROGRAM: MASKS THE BACKGROUND VELOCITY
!
!################################################################################
program MAKE_MASK_RBF
use sep
use mem_mod
use calc_mod
use drlse_mod
implicit none

!===========================================================================
!	Allocate and Initialize
!---------------------------------------------------------------------------
integer                             				:: nz, nx, ix, iz, smoothrad,smoothpass
integer												:: isb, ivb, ibb, nvb, nsb, nbb, ip, i
real												:: aa, dz, dx, oz, ox
logical												:: verbose
real, dimension(:,:), allocatable					:: implicit_surf, count
real, dimension(:,:), allocatable					:: mvb2, maskvelb
double precision, dimension(:,:), allocatable		:: mvb, mvb1, msb, impsz, impsx

!===========================================================================

call sep_init()
call from_param('verbose',verbose,.True.)
if(verbose) write(0,*) "=============== Read in initial parameters =================="
call from_history('n1', nz)
call from_history('n2', nx)
call from_param('smoothrad', smoothrad,2)
call from_param('smoothrad', smoothpass,8)


! Allocate
if(verbose) write(0,*) "=============== Allocate arrays ============================="
allocate(implicit_surf(nz,nx))
allocate(msb(nz,nx))
allocate(count(nz,nx))
allocate(mvb1(nz,nx))
allocate(mvb(nz,nx))
allocate(impsx(nz,nx))
allocate(impsz(nz,nx))

! Initialize
if(verbose) write(0,*) "=============== Initialize =================================="
mvb1 = 0.0
call sep_read(implicit_surf)
call drlse_init(nz, nx, 1.0, 1.0)

! Build full extent smooth masks
if(verbose) write(0,*) "=============== Build smooth masks ==========================="
WHERE (implicit_surf >0.0) mvb1 = 1.0

do i=1,smoothpass
	call smoother(mvb1,mvb,smoothrad)
	mvb1 = mvb
enddo

call gradient(mvb, impsx, impsz)
msb = sqrt(impsx**2 + impsz**2)

! Normalize upper and lower bounds
if(verbose) write(0,*) "=============== Clip smoothed masks ==========================="
msb = (msb - minval(msb))
msb = msb/maxval(msb)
mvb = abs(mvb-1.0)
mvb = (mvb - minval(mvb))
mvb = mvb/maxval(mvb)

! Count the sparse elements
if(verbose) write(0,*) "=============== Count sparse elements ==========================="
count=0.0
WHERE (msb>0.0) count = 1.0
nsb=sum(count)
count=0.0
WHERE (mvb >0.0) count = 1.0
nvb=sum(count)

! Allocate mask
if(verbose) write(0,*) "=============== Allocate sparse mask ==========================="
call mem_alloc2d(maskvelb,nvb,3)

! Make mask sparse
if(verbose) write(0,*) "=============== Make sparse mask ==============================="
ivb=1
do ix=1,nx
	do iz=1,nz
		if ((mvb(iz,ix))>0.0) then
			maskvelb(ivb,1)= mvb(iz,ix)
			maskvelb(ivb,2)=iz
			maskvelb(ivb,3)=ix
			ivb=ivb+1
		endif
	enddo
enddo

if(verbose) write(0,*) "=============== Write outputs ================================="

if (exist_file('maskvelb')) then
	call to_history("n1"    ,nvb				,'maskvelb')
	call to_history("n2"    ,3					,'maskvelb')
	call to_history("label1",'Sparse values'	,'maskvelb')
	call sep_write(SNGL(maskvelb),"maskvelb")
end if

if (exist_file('maskvelbfull')) then
	call to_history("n1"    ,nz					,'maskvelbfull')
	call to_history("n2"    ,nx					,'maskvelbfull')
	call to_history("label1",'Sparse values'	,'maskvelbfull')
	call sep_write(SNGL(mvb),"maskvelbfull")
end if

if (exist_file('masksalt')) then
	call to_history("n1"    ,nz					,'masksalt')
	call to_history("n2"    ,nx					,'masksalt')
	call to_history("label1",'Sparse values'	,'masksalt')
	call sep_write(SNGL(msb),"masksalt")
end if


deallocate(implicit_surf,msb,count,mvb1,mvb,impsx,impsz)

end program
