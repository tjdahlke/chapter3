



module calc_mod
	use mem_mod
	implicit none
	integer, private								:: nz, nx, nt, rec_nx, sou_nx, isou
	double precision, dimension(:,:), allocatable, private  	:: full_vel2
	double precision, private									:: small_number, tdelay, sou_ox, sou_dx, rec_ox, rec_dx, dt, dx, dz, pi, oz, ox, sou_oz
	contains



!/////////////////////////////////////////////////////////////////////////////////////
	subroutine calc_init( nz_in, nx_in, nt_in, rec_nx_in, sou_nx_in, tdelay_in, sou_ox_in, sou_dx_in, rec_ox_in, rec_dx_in, dt_in, dx_in, dz_in, oz_in, ox_in, sou_oz_in)
		integer		:: nz_in, nx_in, nt_in, rec_nx_in, sou_nx_in
		real		:: tdelay_in, sou_ox_in, sou_dx_in, rec_ox_in, rec_dx_in, dt_in, dx_in, dz_in, oz_in, ox_in, sou_oz_in
		nz = nz_in
		nx = nx_in
		nt = nt_in
		rec_nx = rec_nx_in
		sou_nx = sou_nx_in

		tdelay = tdelay_in
		sou_ox = sou_ox_in
		sou_oz = sou_oz_in
		sou_dx = sou_dx_in
		rec_ox = rec_ox_in
		rec_dx = rec_dx_in
		dt = dt_in
		dx = dx_in
		dz = dz_in
		oz = oz_in
		ox = ox_in
	end subroutine




!/////////////////////////////////////////////////////////////////////////////////////
	subroutine calc_vel_init( nz_in, nx_in)
		integer		:: nz_in, nx_in
		nz = nz_in
		nx = nx_in
		! call mem_alloc2d(full_vel2, nz, nx)
		allocate(full_vel2(nz, nx))
	end subroutine



!/////////////////////////////////////////////////////////////////////////////////////
	subroutine smoother(model, smoothmodel, radius)
		double precision, dimension(:,:), target	:: model, smoothmodel
		integer							:: radius, left, right, top, bottom, countr, i, j, k, l
		real							:: val

		if (radius==0) then
			smoothmodel =  model
		else
			do j=1, size(model,2)        ! x axis
				do i=1, size(model,1)    ! z axis

					top    = i - radius
					bottom = i + radius
					left   = j - radius
					right  = j + radius

					if (top    < 1) top  = 1
					if (left   < 1) left = 1

					if (bottom > size(model,1)) bottom = size(model,1)
					if (right  > size(model,2)) right  = size(model,2)

					countr = 0
					val    = 0.0
					do k=top, bottom
						do l=left, right
							countr = countr + 1
							val = val + model(k,l)
						end do
					end do
					smoothmodel(i,j) = real(val)/real(countr)
	 			end do
			end do
		end if
	end subroutine



	!/////////////////////////////////////////////////////////////////////////////////////
		subroutine calc_velocity(phi_in, vel_back, full_vel, vs)
			! double precision, dimension(:,:), target	:: phi_in, full_vel, vel_back
			real, dimension(:,:), target							:: phi_in, full_vel, vel_back
			integer																		:: iz, ix, stencil, ip, np, it
			real																			:: vs

			! Build the smoothing field
			full_vel = vel_back
			do iz=1,nz
				do ix=1,nx
					if (phi_in(iz,ix)>0.0) then
						full_vel(iz,ix) = vs
					end if
				end do
			end do

		end subroutine




end module
