
!###############################################################################
!
!	PICK Radial Basis Fucntion Centers PROGRAM. This is done by choosing a
!	random cloud of Gaussian centers, with more clustering around the areas
!	of the boundaries.
!
!	input:  implicit surface phi NZxNX
!	output1:  mask (preserves exterior) NZxNX
!	output2:  mask (preserves boundary) NZxNX
!
!###############################################################################

program PICK_RBF_CENTERS
use sep
use calc_mod
use drlse_mod
implicit none

!===========================================================================
!	Allocate and Initialize
!---------------------------------------------------------------------------
integer                             				:: nz, nx, ix, iz, smoothrad, smoothpass, ic, ncenters
integer												:: isb, ivb, nvb, nsb, i
real												:: aa, dz, dx, oz, ox, maxprob, minprob, rr
real, dimension(:,:), allocatable					:: phi, centers, rbfcenters
double precision, dimension(:,:), allocatable		:: mvb, mvb1, msb, msb1, impsz, impsx
integer,parameter 									:: seed = 86456
logical												:: verbose
!===============================================================================

call sep_init()
call srand(seed)
call from_history('n1', nz)
call from_history('n2', nx)
call from_history('d1', dz)
call from_history('d2', dx)
call from_history('o1', oz)
call from_history('o2', ox)
call from_param('smoothrad',smoothrad,2)
call from_param('smoothpass',smoothpass,5)
call from_param('maxprob',maxprob,0.075)
call from_param('minprob',minprob,0.005)
call from_param('verbose',verbose,.False.)

! Allocate
allocate(phi(nz,nx))
allocate(msb(nz,nx))
allocate(msb1(nz,nx))
allocate(centers(nz,nx))
allocate(mvb1(nz,nx))
allocate(mvb(nz,nx))
allocate(impsx(nz,nx))
allocate(impsz(nz,nx))
centers=0.0

! Initialize
mvb1 = 0.0
call sep_read(phi)
call drlse_init(nz, nx, 1.0, 1.0)

! Build full extent smooth masks
WHERE (phi >0.0) mvb1 = 1.0
do i=1,smoothpass
	call smoother(mvb1,mvb,smoothrad)
	mvb1 = mvb
enddo
call gradient(mvb, impsx, impsz)
msb1 = sqrt(impsx**2 + impsz**2)

! Normalize upper and lower bounds to some max/min probabilities
msb1 = (msb1 - minval(msb1))
msb1 = msb1/maxval(msb1)*maxprob + minprob
! WHERE (msb1 < minprob) msb1 = minprob
! WHERE (msb1 > maxprob) msb1 = maxprob
! call smoother(msb1,msb,4)
msb=msb1

! Randomly place RBF centers
do iz=1,nz
	do ix=1,nx
		CALL RANDOM_NUMBER(rr)
		if(rr<msb(iz,ix)) then
			centers(iz,ix)=1
		endif
	enddo
enddo
ncenters=sum(centers)
allocate(rbfcenters(ncenters,2))


! Build sparse rbf center array
ic=1
do iz=1,nz
	do ix=1,nx
		if(centers(iz,ix)>0.0) then
			rbfcenters(ic,1) = iz
			rbfcenters(ic,2) = ix
			ic = ic + 1
		endif
	enddo
enddo

! Write outputs
if (exist_file('msb')) then
	call to_history("n1"    ,nz			,'msb')
	call to_history("n2"    ,nx			,'msb')
	call to_history("o1"    ,oz			,'msb')
	call to_history("o2"    ,ox			,'msb')
	call to_history("d1"    ,dz			,'msb')
	call to_history("d2"    ,dx			,'msb')
	call to_history("label1",'z [km]'	,'msb')
	call to_history("label2",'x [km]'	,'msb')
	call sep_write(SNGL(msb),"msb")
end if

if (exist_file('centers')) then
	call to_history("n1"    ,nz			,'centers')
	call to_history("n2"    ,nx			,'centers')
	call to_history("o1"    ,oz			,'centers')
	call to_history("o2"    ,ox			,'centers')
	call to_history("d1"    ,dz			,'centers')
	call to_history("d2"    ,dx			,'centers')
	call to_history("label1",'z [km]'	,'centers')
	call to_history("label2",'x [km]'	,'centers')
	call sep_write(centers,"centers")
end if

call to_history("n1"    ,ncenters)
call to_history("n2"    ,2)
call to_history("o1"    ,0)
call to_history("o2"    ,0)
call to_history("d1"    ,1)
call to_history("d2"    ,1)
call to_history("label1",'# rbf centers')
call to_history("label2",'z, x coord')
call sep_write(rbfcenters)

if(verbose) write(0,*) "============================================"
if(verbose) write(0,*) " Percent reduction = ", 100-100*ncenters/(nz*nx)
if(verbose) write(0,*) "============================================"

end program
