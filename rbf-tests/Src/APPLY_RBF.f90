
!===============================================================================
!		Applies the Radial Basis function
!		MODEL:  Nrbf
!		DATA:  	Nz x Nx
!===============================================================================

program APPLY_RBF
	use sep
	use rbf_mod
	implicit none

	integer									:: nrbf, nz, nx, trad
	real									:: timer_sum=0.
	real, dimension(:,:), allocatable		:: data, rbfcoord, rbftable
	real, dimension(:), allocatable			:: model
	logical									:: verbose, adjoint
	integer, dimension(8)               	:: timer0, timer1

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	call from_param("verbose", verbose, .TRUE.)
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("n1", nz)
	call from_param("n2", nx)
	call from_param("adjoint", adjoint, .FALSE.)
	call from_param("trad", trad, 15)
	call from_aux("rbfcoord", "n1", nrbf)

	allocate(model(nrbf))
	allocate(data(nz,nx))
	allocate(rbfcoord(nrbf,2))
	allocate(rbftable((2*trad+1),(2*trad+1)))

	if (adjoint) then
		if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Reading in RBF weights <<<<<<<<<<<<<<<<<<<<<<<"
		call sep_read(data)
	else
		if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Reading in RBF surface <<<<<<<<<<<<<<<<<<<<<<<"
		call sep_read(model)
	endif
	call sep_read(rbfcoord,"rbfcoord")
	call sep_read(rbftable,"rbftable")


!##########################################################################################
	call rbf_init(nz, nx, nrbf, trad, rbfcoord, rbftable)
	call rbf(adjoint,.False.,model,data)
	if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(model) = ",sum(model)
	if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(data) = ",sum(data)
!##########################################################################################

	if (adjoint) then
		if(verbose) write(0,*) "========= Write out RBF file (weights) ================="
		call to_history("n1",nrbf)
		call to_history("n2",1)
		call to_history("label1",'# sparse rbf centers')
		call sep_write(model)
		if(verbose) write(0,*) "========= sum(model) = ", sum(model)
	else
		if(verbose) write(0,*) "========= Write out RBF file (implicit surface) ==+====="
		call to_history("n1",nz)
		call to_history("n2",nx)
		call to_history("label1",'z [km]')
		call to_history("label2",'x [km]')
		call sep_write(data)
	end if

	if (verbose) write(0,*) "APPLY_RBF program complete"
	deallocate(data,model)

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec)", timer_sum/1000
	write(0,*) "timer sum (min)", timer_sum/1000/60
	write(0,*) "DONE!"

end program
