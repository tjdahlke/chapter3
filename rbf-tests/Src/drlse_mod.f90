!==============================================================
!	DOUBLE PRECISION
!==============================================================


module drlse_mod
use sep
use mem_mod
implicit none
real, private    										:: pi, small_number, dr, dc, h
integer, private    									:: nr, nc, i, j, k
double precision, allocatable, dimension(:,:), private	:: phi_a, phi_x, phi_y, s, Nxx, Nyy, curvature, laplac_mat

contains
!==============================================================



subroutine drlse_init(nr_in, nc_in, dr_in, dc_in) !(nz,nx,dz,dx)
	integer           					:: nr_in, nc_in
	real								:: dr_in, dc_in

	nr = nr_in
	nc = nc_in
	dr = dr_in
	dc = dc_in

	if (dr .ne. dc) then
		write(0,*) " The grid spacing (dx & dz) should be equal and they aren't!"
		call exit
	end if

	h=dr
	pi = 3.1415926535897932384626433832795028841971
	small_number=0.0000000001

	! call mem_alloc2d(phi_a, nr, nc)
	! call mem_alloc2d(phi_x, nr, nc)
	! call mem_alloc2d(phi_y, nr, nc)
	! call mem_alloc2d(curvature, nr, nc)
	! call mem_alloc2d(laplac_mat, nr, nc)
	! call mem_alloc2d(Nxx, nr, nc)
	! call mem_alloc2d(Nyy, nr, nc)

	allocate(phi_a(nr, nc))
	allocate(phi_x(nr, nc))
	allocate(phi_y(nr, nc))
	allocate(curvature(nr, nc))
	allocate(laplac_mat(nr, nc))
	allocate(Nxx(nr, nc))
	allocate(Nyy(nr, nc))

end subroutine





!/////////////////////////////////////////////////////////////////////////////////////
subroutine calc_drlse_term( phi_in, drlse_term, s ,singlewell)
	double precision, dimension(:,:), target	:: s, phi_in, drlse_term
	logical							:: singlewell

!----------- Create nabla term (gradient of phi with respect to x and y) ------------
	call neumann_bound_cond(phi_in, phi_a)
	call gradient(phi_a, phi_x, phi_y)
	s = sqrt( phi_x**2 + phi_y**2) ! Nabla term?

!-----------  Calculate DRLSE term  -------------------------------------------------
	Nxx = phi_x/(s + small_number)
	Nyy = phi_y/(s + small_number)

	if (singlewell) then
!-----------  Single-well DRLSE  -------------------------------------------------
		call div(Nxx,Nyy,curvature)
		call del2(phi_a,laplac_mat)
		drlse_term = (4*laplac_mat - curvature)
	else
!-----------  Double-well DRLSE  -------------------------------------------------
		call dist_reg_p2(phi_a, drlse_term)
	end if

end subroutine







subroutine drlse_edge(phi_0, g, lambda, mu, alpha, epsilon, time_step, num_iter, double_well, phi)
double precision, dimension(:,:) 			:: phi_0, g, phi
double precision, dimension(nr,nc) 			:: vx, vy, phi_x, phi_y, s, Nx, Ny, curvature, distRegTerm, laplac_mat, dirac_phi, areaTerm, edgeTerm, phi_in, phi_out
integer               			:: num_iter!, time_step
real					:: lambda, mu, alpha, epsilon, time_step
logical					:: double_well

vx=0.0; vy=0.0; phi_x=0.0; phi_y=0.0; s=0.0; Nx=0.0; Ny=0.0; curvature=0.0; distRegTerm=0.0; laplac_mat=0.0; dirac_phi=0.0; areaTerm=0.0; edgeTerm=0.0; phi_in=0.0; phi_out=0.0; phi=0.0;

phi=phi_0
 call gradient(g, vx, vy)

do k=1,num_iter
	phi_in=phi

	call neumann_bound_cond(phi_in,phi_out)

	call gradient(phi_out, phi_x, phi_y)
	s = sqrt( phi_x**2 + phi_y**2)
	Nx = phi_x/(s + small_number)
	Ny = phi_y/(s + small_number)
	call div(Nx,Ny,curvature)

	!Using single well
	call del2(phi_out,laplac_mat)
	distRegTerm = 4*laplac_mat - curvature

	call dirac(phi_out,epsilon,dirac_phi)
	areaTerm = dirac_phi * g ! Balloon / pressure force
	edgeTerm = dirac_phi * ((vx * Nx) + (vy * Ny)) + dirac_phi * g * curvature
	phi = phi_out + time_step*(mu*distRegTerm + lambda*edgeTerm + alpha*areaTerm)
end do
end subroutine




subroutine signmap(phi,map_of_sign)
double precision, dimension (:,:)  	:: phi,map_of_sign
map_of_sign=0.0;

do j=1,nc
    do i=1,nr
	if ( phi(i,j) >= 0.0 ) then
		map_of_sign(i,j) =  1.0
	else
		map_of_sign(i,j) = -1.0
	end if
    end do
end do
end subroutine






subroutine gradient(nt_in, nt_dx, nt_dy)
double precision, dimension(:,:)			:: nt_in, nt_dx, nt_dy
nt_dx=0.0
nt_dy=0.0

do j=1,nc
    do i=1,nr
		! Middle gradient values
		if(j/=1 .and. j/=nc) then
			nt_dx(i,j) = (nt_in(i,j+1) - nt_in(i,j-1) )/(2*h)
		endif
		if(i/=1 .and. i/=nr) then
			nt_dy(i,j) = (nt_in(i+1,j) - nt_in(i-1,j) )/(2*h)
		endif
		! Edge dx gradient values
		if(j==1) then
			nt_dx(i,1) = ( nt_in(i,2) - nt_in(i,1) )/h
		endif
		if(j==nc) then
			nt_dx(i,nc) = ( nt_in(i,nc) - nt_in(i,nc-1) )/h
		endif
    end do
    ! Edge dy gradient values
    nt_dy(1,j)  = ( nt_in(2,j) - nt_in(1,j) )/h
    nt_dy(nr,j) = ( nt_in(nr,j) - nt_in(nr-1,j) )/h
end do
end subroutine





subroutine gradientR(nt_in, nt_dx, nt_dy)
	real, dimension(:,:)	:: nt_in, nt_dx, nt_dy
	nt_dx=0.0
	nt_dy=0.0

	do j=1,nc
	    do i=1,nr
			! Middle gradient values
			if(j/=1 .and. j/=nc) then
				nt_dx(i,j) = (nt_in(i,j+1) - nt_in(i,j-1) )/(2*h)
			endif
			if(i/=1 .and. i/=nr) then
				nt_dy(i,j) = (nt_in(i+1,j) - nt_in(i-1,j) )/(2*h)
			endif
			! Edge dx gradient values
			if(j==1) then
				nt_dx(i,1) = ( nt_in(i,2) - nt_in(i,1) )/h
			endif
			if(j==nc) then
				nt_dx(i,nc) = ( nt_in(i,nc) - nt_in(i,nc-1) )/h
			endif
	    end do
	    ! Edge dy gradient values
	    nt_dy(1,j)  = ( nt_in(2,j) - nt_in(1,j) )/h
	    nt_dy(nr,j) = ( nt_in(nr,j) - nt_in(nr-1,j) )/h
	end do
	write(0,*) "sum(nt_dx) =",sum(nt_dx)
	write(0,*) "sum(nt_dy) =",sum(nt_dy)
	write(0,*) "sum(nt_in) =",sum(nt_in)

end subroutine






subroutine del2(in, out)
double precision, dimension(:,:) 			:: in, out
double precision, dimension(nr,nc) 			:: interior, left, right, top, bottom, ul_corner, br_corner
integer               			:: i, j
real					:: a, ul, ur, bl, br
! Zero out internal arrays
out = 0.0; interior = 0.0; left = 0.0;  right = 0.0;  top = 0.0;  bottom = 0.0;  ul_corner = 0.0;  br_corner = 0.0;
a=0.25

! Interior Points
do j=1,nc
    do i=1,nr
	! Interior Point Calculations
	if( j>1 .and. j<nc .and. i>1 .and. i<nr )then
	interior(i,j) = a*((in(i-1,j) + in(i+1,j) + in(i,j-1) + in(i,j+1)) - 4*in(i,j) )/(h**2)
	end if
	! Boundary Conditions for Left and Right edges
	left(i,1) = a*(-5.0*in(i,2) + 4.0*in(i,3) - in(i,4) + 2.0*in(i,1) + in(i+1,1) + in(i-1,1) - 2.0*in(i,1) )/(h**2)
	right(i,nc) = a*(-5.0*in(i,nc-1) + 4.0*in(i,nc-2) - in(i,nc-3) + 2.0*in(i,nc) + in(i+1,nc) + in(i-1,nc) - 2.0*in(i,nc) )/(h**2)
    end do
    ! Boundary Conditions for Top and Bottom edges
    top(1,j) = a*(-5.0*in(2,j) + 4.0*in(3,j) - in(4,j) + 2.0*in(1,j) + in(1,j+1) + in(1,j-1) - 2.0*in(1,j) )/(h**2)
    bottom(nr,j) = a*(-5.0*in(nr-1,j) + 4.0*in(nr-2,j) - in(nr-3,j) + 2.0*in(nr,j) + in(nr,j+1) + in(nr,j-1) - 2.0*in(nr,j) )/(h**2)
end do
out = interior + left + right + top + bottom
! Calculate BC for the corners
ul = a*(-5.0*in(1,2) + 4.0*in(1,3) - in(1,4) + 2.0*in(1,1) - 5.0*in(2,1) + 4.0*in(3,1) - in(4,1) + 2.0*in(1,1))/(h**2)
br = a*(-5.0*in(nr,nc-1) + 4.0*in(nr,nc-2) - in(nr,nc-3) + 2.0*in(nr,nc) - 5.0*in(nr-1,nc) + 4.0*in(nr-2,nc) - in(nr-3,nc) + 2.0*in(nr,nc))/(h**2)
bl = a*(-5.0*in(nr,2) + 4.0*in(nr,3) - in(nr,4) + 2.0*in(nr,1) - 5.0*in(nr-1,1) + 4.0*in(nr-2,1) - in(nr-3,1) + 2.0*in(nr,1))/(h**2)
ur = a*(-5.0*in(1,nc-1) + 4.0*in(1,nc-2) - in(1,nc-3) + 2.0*in(1,nc) - 5.0*in(2,nc) + 4.0*in(3,nc) - in(4,nc) + 2.0*in(1,nc))/(h**2)
! Apply BC for the corners
out(1,1)=ul
out(1,nc)=ur
out(nr,1)=bl
out(nr,nc)=br
end subroutine







subroutine div(nx,ny,out)
double precision, dimension (:,:)  			:: nx, ny, out
double precision, dimension (nr,nc)			:: outx, outy, junk
outx=0.0; outy=0.0; junk=0.0; out=0.0;
 call gradient(nx,outx,junk)
 call gradient(ny,junk,outy)
out=outx+outy
end subroutine




subroutine neumann_bound_cond(in,out)
double precision, dimension(:,:)			:: in, out
out=0.0
out=in
out((/ 1, nr /), (/ 1, nc /)) = out((/ 3, nr-2 /), (/ 3, nc-2 /))
out((/ 1, nr /), 2:(nc-1)) = out((/ 3, nr-2 /), 2:(nc-1))
out(2:(nr-1), (/ 1, nc /)) = out(2:(nr-1), (/ 3, nc-2 /))
end subroutine





subroutine dirac(x,sigma,out)
real										:: sigma
double precision, dimension (:,:)  			:: x, out
double precision, dimension (nr,nc)			:: a
a=0; out=0.0;
out=(1/(2*sigma))*(1 + cos(pi*x/sigma))
! Make binary selector matrices
do j=1,nc
    do i=1,nr
	if(x(i,j) <= sigma .and. x(i,j) >= -sigma) then
		a(i,j) = 1 ! "a" matrix
	endif
    end do
end do
out=out*a
end subroutine







subroutine dist_reg_p2(phi,out)
double precision, dimension(:,:)			:: phi, out
double precision, dimension (nr,nc)			:: phi_x, phi_y, a, b, c, d, e, f, s, ps, dps, div_inx, div_iny, lap, temp
 phi_x=0.0; phi_y=0.0; a=0.0; b=0.0; c=0.0; d=0.0; e=0.0; f=0.0; s=0.0; ps=0.0; dps=0.0; div_inx=0.0; div_iny=0.0; lap=0.0; temp=0.0; out=0.0;

 call gradient(phi,phi_x,phi_y)
s=sqrt(phi_x**2 + phi_y**2)
! Make binary selector matrices
do j=1,nc
    do i=1,nr
	if(s(i,j) >= 0 .and.  s(i,j) <= 1) then
		a(i,j) = 1   ! "a" matrix
	end if
	if(s(i,j) > 1 ) then
		b(i,j) = 1   ! "b" matrix
	end if
	if(s(i,j) == 0 ) then
		c(i,j) = 1   ! "c" matrix
		else
		d(i,j) = 1   ! "d" matrix
	end if
		ps(i,j)=a(i,j)*sin(2*pi*s(i,j))/(2*pi)+b(i,j)*(s(i,j)-1)
	if(ps(i,j) == 0 ) then
		e(i,j) = 1   ! "e" matrix
		else
		f(i,j) = 1   ! "f" matrix
	end if
	dps(i,j)=(f(i,j)*ps(i,j) + e(i,j))/(d(i,j)*s(i,j) + c(i,j))
    end do
end do
div_inx = (dps*phi_x - phi_x)
div_iny = (dps*phi_y - phi_y)
 call div(div_inx,div_iny,temp)
 call del2(phi, lap)
out =  temp + 4*lap ! The lap term is what doesnt match the MATLAB code this is based on. This is because a different Laplacian operator is being used. temp is fine though, no problems there.
end subroutine




end module








! !==============================================================

! ! These are modules that are used for performing DRLSE (Distance Regularized Level Set Evolution). They were orignally written in MATLAB by Chunming Li (lchunming@gmail.com, i_chunming@hotmail.com ), and converted into fortran by Taylor Dahlke (Stanford University).


! ! This implementation is based upon the following paper:
! ! C. Li, C. Xu, C. Gui, M. D. Fox, "Distance Regularized Level Set Evolution and Its Application to Image Segmentation", IEEE Trans. Image Processing, vol. 19 (12), pp.3243-3254, 2010.

! !==============================================================
! module drlse_mod
! use sep
! use mem_mod
! implicit none
! real, private    							:: pi, small_number, dr, dc, h, dt
! integer, private    						:: nr, nc, i, j, k, nt
! real, allocatable, dimension(:,:), private	:: phi_a, phi_x, phi_y, s, Nxx, Nyy, curvature, laplac_mat

! contains
! !==============================================================



! subroutine drlse_init(nr_in, nc_in, nt_in, dr_in, dc_in, dt_in) !(nz,nx,dz,dx)
! 	integer           					:: nr_in, nc_in, nt_in
! 	real								:: dr_in, dc_in, dt_in

! 	nr = nr_in
! 	nc = nc_in
! 	dr = dr_in
! 	dc = dc_in
! 	nt = nt_in
! 	dt = dt_in

! 	if (dr .ne. dc) then
! 		write(0,*) " The grid spacing (dx & dz) should be equal and they aren't!"
! 		call exit
! 	end if

! 	h=dr
! 	pi = 3.1415926535897932384626433832795028841971
! 	small_number=0.0000000001

! 	call mem_alloc2d(phi_a, nr, nc)
! 	call mem_alloc2d(phi_x, nr, nc)
! 	call mem_alloc2d(phi_y, nr, nc)
! 	call mem_alloc2d(curvature, nr, nc)
! 	call mem_alloc2d(laplac_mat, nr, nc)
! 	call mem_alloc2d(Nxx, nr, nc)
! 	call mem_alloc2d(Nyy, nr, nc)

! end subroutine





! !/////////////////////////////////////////////////////////////////////////////////////
! subroutine calc_drlse_term( phi_in, drlse_term, s ,singlewell)
! 	real, dimension(:,:), target	:: s, phi_in, drlse_term
! 	logical							:: singlewell

! !----------- Create nabla term (gradient of phi with respect to x and y) ------------
! 	call neumann_bound_cond(phi_in, phi_a)
! 	call gradient(phi_a, phi_x, phi_y)
! 	s = sqrt( phi_x**2 + phi_y**2) ! Nabla term?

! !-----------  Calculate DRLSE term  -------------------------------------------------
! 	Nxx = phi_x/(s + small_number)
! 	Nyy = phi_y/(s + small_number)

! 	if (singlewell) then
! !-----------  Single-well DRLSE  -------------------------------------------------
! 		call div(Nxx,Nyy,curvature)
! 		call del2(phi_a,laplac_mat)
! 		drlse_term = (4*laplac_mat - curvature)
! 	else
! !-----------  Double-well DRLSE  -------------------------------------------------
! 		call dist_reg_p2(phi_a, drlse_term)
! 	end if

! end subroutine







! subroutine drlse_edge(phi_0, g, lambda, mu, alpha, epsilon, time_step, num_iter, double_well, phi)
! real, dimension(:,:) 			:: phi_0, g, phi
! real, dimension(nr,nc) 			:: vx, vy, phi_x, phi_y, s, Nx, Ny, curvature, distRegTerm, laplac_mat, dirac_phi, areaTerm, edgeTerm, phi_in, phi_out
! integer               			:: num_iter!, time_step
! real					:: lambda, mu, alpha, epsilon, time_step
! logical					:: double_well

! vx=0.0; vy=0.0; phi_x=0.0; phi_y=0.0; s=0.0; Nx=0.0; Ny=0.0; curvature=0.0; distRegTerm=0.0; laplac_mat=0.0; dirac_phi=0.0; areaTerm=0.0; edgeTerm=0.0; phi_in=0.0; phi_out=0.0; phi=0.0;

! phi=phi_0
!  call gradient(g, vx, vy)

! do k=1,num_iter
! 	phi_in=phi

! 	call neumann_bound_cond(phi_in,phi_out)

! 	call gradient(phi_out, phi_x, phi_y)
! 	s = sqrt( phi_x**2 + phi_y**2)
! 	Nx = phi_x/(s + small_number)
! 	Ny = phi_y/(s + small_number)
! 	call div(Nx,Ny,curvature)

! 	!Using single well
! 	call del2(phi_out,laplac_mat)
! 	distRegTerm = 4*laplac_mat - curvature

! 	call dirac(phi_out,epsilon,dirac_phi)
! 	areaTerm = dirac_phi * g ! Balloon / pressure force
! 	edgeTerm = dirac_phi * ((vx * Nx) + (vy * Ny)) + dirac_phi * g * curvature
! 	phi = phi_out + time_step*(mu*distRegTerm + lambda*edgeTerm + alpha*areaTerm)
! end do
! end subroutine




! subroutine signmap(phi,map_of_sign)
! real, dimension (:,:)  	:: phi,map_of_sign
! map_of_sign=0.0;

! do j=1,nc
!     do i=1,nr
! 	if ( phi(i,j) >= 0.0 ) then
! 		map_of_sign(i,j) =  1.0
! 	else
! 		map_of_sign(i,j) = -1.0
! 	end if
!     end do
! end do
! end subroutine






! subroutine gradient(nt_in, nt_dx, nt_dy)
! real, dimension(:,:)			:: nt_in, nt_dx, nt_dy
! nt_dx=0.0
! nt_dy=0.0

! do j=1,nc
!     do i=1,nr
! 		! Middle gradient values
! 		if(j/=1 .and. j/=nc) then
! 			nt_dx(i,j) = (nt_in(i,j+1) - nt_in(i,j-1) )/(2*h)
! 		endif
! 		if(i/=1 .and. i/=nr) then
! 			nt_dy(i,j) = (nt_in(i+1,j) - nt_in(i-1,j) )/(2*h)
! 		endif
! 		! Edge dx gradient values
! 		if(j==1) then
! 			nt_dx(i,1) = ( nt_in(i,2) - nt_in(i,1) )/h
! 		endif
! 		if(j==nc) then
! 			nt_dx(i,nc) = ( nt_in(i,nc) - nt_in(i,nc-1) )/h
! 		endif
!     end do
!     ! Edge dy gradient values
!     nt_dy(1,j)  = ( nt_in(2,j) - nt_in(1,j) )/h
!     nt_dy(nr,j) = ( nt_in(nr,j) - nt_in(nr-1,j) )/h
! end do
! end subroutine







! subroutine del2(in, out)
! real, dimension(:,:) 			:: in, out
! real, dimension(nr,nc) 			:: interior, left, right, top, bottom, ul_corner, br_corner
! integer               			:: i, j
! real					:: a, ul, ur, bl, br
! ! Zero out internal arrays
! out = 0.0; interior = 0.0; left = 0.0;  right = 0.0;  top = 0.0;  bottom = 0.0;  ul_corner = 0.0;  br_corner = 0.0;
! a=0.25

! ! Interior Points
! do j=1,nc
!     do i=1,nr
! 	! Interior Point Calculations
! 	if( j>1 .and. j<nc .and. i>1 .and. i<nr )then
! 	interior(i,j) = a*((in(i-1,j) + in(i+1,j) + in(i,j-1) + in(i,j+1)) - 4*in(i,j) )/(h**2)
! 	end if
! 	! Boundary Conditions for Left and Right edges
! 	left(i,1) = a*(-5.0*in(i,2) + 4.0*in(i,3) - in(i,4) + 2.0*in(i,1) + in(i+1,1) + in(i-1,1) - 2.0*in(i,1) )/(h**2)
! 	right(i,nc) = a*(-5.0*in(i,nc-1) + 4.0*in(i,nc-2) - in(i,nc-3) + 2.0*in(i,nc) + in(i+1,nc) + in(i-1,nc) - 2.0*in(i,nc) )/(h**2)
!     end do
!     ! Boundary Conditions for Top and Bottom edges
!     top(1,j) = a*(-5.0*in(2,j) + 4.0*in(3,j) - in(4,j) + 2.0*in(1,j) + in(1,j+1) + in(1,j-1) - 2.0*in(1,j) )/(h**2)
!     bottom(nr,j) = a*(-5.0*in(nr-1,j) + 4.0*in(nr-2,j) - in(nr-3,j) + 2.0*in(nr,j) + in(nr,j+1) + in(nr,j-1) - 2.0*in(nr,j) )/(h**2)
! end do
! out = interior + left + right + top + bottom
! ! Calculate BC for the corners
! ul = a*(-5.0*in(1,2) + 4.0*in(1,3) - in(1,4) + 2.0*in(1,1) - 5.0*in(2,1) + 4.0*in(3,1) - in(4,1) + 2.0*in(1,1))/(h**2)
! br = a*(-5.0*in(nr,nc-1) + 4.0*in(nr,nc-2) - in(nr,nc-3) + 2.0*in(nr,nc) - 5.0*in(nr-1,nc) + 4.0*in(nr-2,nc) - in(nr-3,nc) + 2.0*in(nr,nc))/(h**2)
! bl = a*(-5.0*in(nr,2) + 4.0*in(nr,3) - in(nr,4) + 2.0*in(nr,1) - 5.0*in(nr-1,1) + 4.0*in(nr-2,1) - in(nr-3,1) + 2.0*in(nr,1))/(h**2)
! ur = a*(-5.0*in(1,nc-1) + 4.0*in(1,nc-2) - in(1,nc-3) + 2.0*in(1,nc) - 5.0*in(2,nc) + 4.0*in(3,nc) - in(4,nc) + 2.0*in(1,nc))/(h**2)
! ! Apply BC for the corners
! out(1,1)=ul
! out(1,nc)=ur
! out(nr,1)=bl
! out(nr,nc)=br
! end subroutine







! subroutine div(nx,ny,out)
! real, dimension (:,:)  			:: nx, ny, out
! real, dimension (nr,nc)			:: outx, outy, junk
! outx=0.0; outy=0.0; junk=0.0; out=0.0;
!  call gradient(nx,outx,junk)
!  call gradient(ny,junk,outy)
! out=outx+outy
! end subroutine




! subroutine neumann_bound_cond(in,out)
! real, dimension(:,:)			:: in, out
! out=0.0
! out=in
! out((/ 1, nr /), (/ 1, nc /)) = out((/ 3, nr-2 /), (/ 3, nc-2 /))
! out((/ 1, nr /), 2:(nc-1)) = out((/ 3, nr-2 /), 2:(nc-1))
! out(2:(nr-1), (/ 1, nc /)) = out(2:(nr-1), (/ 3, nc-2 /))
! end subroutine





! subroutine dirac(x,sigma,out)
! real					:: sigma
! real, dimension (:,:)  			:: x, out
! real, dimension (nr,nc)			:: a
! a=0; out=0.0;
! out=(1/(2*sigma))*(1 + cos(pi*x/sigma))
! ! Make binary selector matrices
! do j=1,nc
!     do i=1,nr
! 	if(x(i,j) <= sigma .and. x(i,j) >= -sigma) then
! 		a(i,j) = 1 ! "a" matrix
! 	endif
!     end do
! end do
! out=out*a
! end subroutine







! subroutine dist_reg_p2(phi,out)
! real, dimension(:,:)			:: phi, out
! real, dimension (nr,nc)			:: phi_x, phi_y, a, b, c, d, e, f, s, ps, dps, div_inx, div_iny, lap, temp
!  phi_x=0.0; phi_y=0.0; a=0.0; b=0.0; c=0.0; d=0.0; e=0.0; f=0.0; s=0.0; ps=0.0; dps=0.0; div_inx=0.0; div_iny=0.0; lap=0.0; temp=0.0; out=0.0;

!  call gradient(phi,phi_x,phi_y)
! s=sqrt(phi_x**2 + phi_y**2)
! ! Make binary selector matrices
! do j=1,nc
!     do i=1,nr
! 	if(s(i,j) >= 0 .and.  s(i,j) <= 1) then
! 		a(i,j) = 1   ! "a" matrix
! 	end if
! 	if(s(i,j) > 1 ) then
! 		b(i,j) = 1   ! "b" matrix
! 	end if
! 	if(s(i,j) == 0 ) then
! 		c(i,j) = 1   ! "c" matrix
! 		else
! 		d(i,j) = 1   ! "d" matrix
! 	end if
! 		ps(i,j)=a(i,j)*sin(2*pi*s(i,j))/(2*pi)+b(i,j)*(s(i,j)-1)
! 	if(ps(i,j) == 0 ) then
! 		e(i,j) = 1   ! "e" matrix
! 		else
! 		f(i,j) = 1   ! "f" matrix
! 	end if
! 	dps(i,j)=(f(i,j)*ps(i,j) + e(i,j))/(d(i,j)*s(i,j) + c(i,j))
!     end do
! end do
! div_inx = (dps*phi_x - phi_x)
! div_iny = (dps*phi_y - phi_y)
!  call div(div_inx,div_iny,temp)
!  call del2(phi, lap)
! out =  temp + 4*lap ! The lap term is what doesnt match the MATLAB code this is based on. This is because a different Laplacian operator is being used. temp is fine though, no problems there.
! end subroutine




! end module
